﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Totus_Tuus.Classes;
using Totus_Tuus.Classes.usuario;
using Totus_Tuus.Telas;

namespace Totus_Tuus
{
    public partial class FMRlogin : Form
    {
        public FMRlogin()
        {
            InitializeComponent();
        }

        private void BTNentrar_Click(object sender, EventArgs e)
        {
            DTOusuario dto = new DTOusuario();

            BusinessUsuario bus = new BusinessUsuario();
            dto.Email = TXTentrarEmail.Text;
            dto.Senha_Usuario = TXTentrarSenha.Text;

                    
                
            dto = bus.Logou(dto);
            bool entrar = dto.Entrou;
            if (entrar == true)
            {
                SessaoUsuarioLogado.UsuarioLogado = dto;

                MessageBox.Show("Login efetuado com sucesso", "Uhuu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TelaInicial telinha = new TelaInicial();
                telinha.Show();

                this.Close();
            }
            else
            {
                MessageBox.Show("Credênciais Inválidas", "oops", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
            


        }

        private void button1_Click(object sender, EventArgs e)
        {
            FRMusuario user = new FRMusuario();
            user.Show();
            this.Hide();


        }
    }
}
