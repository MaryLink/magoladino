﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Totus_Tuus.Telas.Santos
{
    public partial class FRMlivros : UserControl
    {
        public FRMlivros()
        {
            InitializeComponent();
        }


        private void BTNsãoLuís_Click_1(object sender, EventArgs e)
        {
            FRMmaria tela = new FRMmaria("São Luís Maria");
            FRMlivros livros = new FRMlivros();

            if (panelLivros.Controls.Count > 5)
            {
                panelLivros.Controls.RemoveAt(panelLivros.Controls.Count - 1);
            }

            panelLivros.Controls.Add(tela);

            lblLIVROS.Visible = false;
            lblSL.Visible = false;
            lblJoaodaCruz.Visible = false;
            lblJME.Visible = false;
            BTNsãoJosemaria.Visible = false;
            BTNsãoJoãodaCruz.Visible = false;
            BTNsãoLuís.Visible = false;

            panelLivros.BackgroundImage = Properties.Resources.são_luis_desfoc;
            tela.BackgroundImage = Properties.Resources.são_luis_desfoc;
        }

        private void BTNsãoJosemaria_Click(object sender, EventArgs e)
        {
            FRMmaria tela = new FRMmaria("São JóseMaria Escrivá");
            FRMlivros livros = new FRMlivros();

            if (panelLivros.Controls.Count > 5)
            {
                panelLivros.Controls.RemoveAt(panelLivros.Controls.Count - 1);
            }

            panelLivros.Controls.Add(tela);

            lblLIVROS.Visible = false;
            lblSL.Visible = false;
            lblJoaodaCruz.Visible = false;
            lblJME.Visible = false;
            BTNsãoJosemaria.Visible = false;
            BTNsãoJoãodaCruz.Visible = false;
            BTNsãoLuís.Visible = false;

            panelLivros.BackgroundImage = Properties.Resources.sao_josemaria;
            tela.BackgroundImage = Properties.Resources.sao_josemaria;

        }

        private void BTNsãoJoãodaCruz_Click(object sender, EventArgs e)
        {
            FRMmaria tela = new FRMmaria("São João da Cruz");
            FRMlivros livros = new FRMlivros();

            if (panelLivros.Controls.Count > 5)
            {
                panelLivros.Controls.RemoveAt(panelLivros.Controls.Count - 1);
            }

            panelLivros.Controls.Add(tela);

            lblLIVROS.Visible = false;
            lblSL.Visible = false;
            lblJoaodaCruz.Visible = false;
            lblJME.Visible = false;
            BTNsãoJosemaria.Visible = false;
            BTNsãoJoãodaCruz.Visible = false;
            BTNsãoLuís.Visible = false;

            panelLivros.BackgroundImage = Properties.Resources.são_joão_da_cruz;
            tela.BackgroundImage = Properties.Resources.são_joão_da_cruz;
            
        }
    }
}
