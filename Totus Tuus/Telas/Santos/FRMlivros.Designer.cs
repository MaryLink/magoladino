﻿namespace Totus_Tuus.Telas.Santos
{
    partial class FRMlivros
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLivros = new System.Windows.Forms.Panel();
            this.lblJoaodaCruz = new System.Windows.Forms.Label();
            this.lblSL = new System.Windows.Forms.Label();
            this.lblJME = new System.Windows.Forms.Label();
            this.lblLIVROS = new System.Windows.Forms.Label();
            this.BTNsãoJoãodaCruz = new System.Windows.Forms.PictureBox();
            this.BTNsãoJosemaria = new System.Windows.Forms.PictureBox();
            this.BTNsãoLuís = new System.Windows.Forms.PictureBox();
            this.panelLivros.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTNsãoJoãodaCruz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNsãoJosemaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNsãoLuís)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLivros
            // 
            this.panelLivros.BackgroundImage = global::Totus_Tuus.Properties.Resources.rosa_branca;
            this.panelLivros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelLivros.Controls.Add(this.lblJoaodaCruz);
            this.panelLivros.Controls.Add(this.lblSL);
            this.panelLivros.Controls.Add(this.lblJME);
            this.panelLivros.Controls.Add(this.lblLIVROS);
            this.panelLivros.Controls.Add(this.BTNsãoJoãodaCruz);
            this.panelLivros.Controls.Add(this.BTNsãoJosemaria);
            this.panelLivros.Controls.Add(this.BTNsãoLuís);
            this.panelLivros.Location = new System.Drawing.Point(0, 0);
            this.panelLivros.Name = "panelLivros";
            this.panelLivros.Size = new System.Drawing.Size(817, 417);
            this.panelLivros.TabIndex = 0;
            // 
            // lblJoaodaCruz
            // 
            this.lblJoaodaCruz.AutoSize = true;
            this.lblJoaodaCruz.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJoaodaCruz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblJoaodaCruz.Font = new System.Drawing.Font("Monotype Corsiva", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblJoaodaCruz.ForeColor = System.Drawing.Color.White;
            this.lblJoaodaCruz.Location = new System.Drawing.Point(593, 317);
            this.lblJoaodaCruz.Margin = new System.Windows.Forms.Padding(8);
            this.lblJoaodaCruz.Name = "lblJoaodaCruz";
            this.lblJoaodaCruz.Size = new System.Drawing.Size(96, 20);
            this.lblJoaodaCruz.TabIndex = 14;
            this.lblJoaodaCruz.Text = "João da Cruz";
            // 
            // lblSL
            // 
            this.lblSL.AutoSize = true;
            this.lblSL.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSL.Font = new System.Drawing.Font("Monotype Corsiva", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblSL.ForeColor = System.Drawing.Color.White;
            this.lblSL.Location = new System.Drawing.Point(77, 318);
            this.lblSL.Margin = new System.Windows.Forms.Padding(8);
            this.lblSL.Name = "lblSL";
            this.lblSL.Size = new System.Drawing.Size(141, 19);
            this.lblSL.TabIndex = 13;
            this.lblSL.Text = "Luis Maria Grignion";
            // 
            // lblJME
            // 
            this.lblJME.AutoSize = true;
            this.lblJME.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJME.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblJME.Font = new System.Drawing.Font("Monotype Corsiva", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblJME.ForeColor = System.Drawing.Color.White;
            this.lblJME.Location = new System.Drawing.Point(330, 317);
            this.lblJME.Margin = new System.Windows.Forms.Padding(8);
            this.lblJME.Name = "lblJME";
            this.lblJME.Size = new System.Drawing.Size(128, 20);
            this.lblJME.TabIndex = 12;
            this.lblJME.Text = "Josemaría Escrivá";
            // 
            // lblLIVROS
            // 
            this.lblLIVROS.AutoSize = true;
            this.lblLIVROS.Font = new System.Drawing.Font("Monotype Corsiva", 25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblLIVROS.ForeColor = System.Drawing.Color.White;
            this.lblLIVROS.Location = new System.Drawing.Point(31, 44);
            this.lblLIVROS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLIVROS.Name = "lblLIVROS";
            this.lblLIVROS.Size = new System.Drawing.Size(258, 41);
            this.lblLIVROS.TabIndex = 11;
            this.lblLIVROS.Text = "Livros Dos Santos";
            // 
            // BTNsãoJoãodaCruz
            // 
            this.BTNsãoJoãodaCruz.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BTNsãoJoãodaCruz.Image = global::Totus_Tuus.Properties.Resources.são_joão_da_cruz;
            this.BTNsãoJoãodaCruz.Location = new System.Drawing.Point(593, 125);
            this.BTNsãoJoãodaCruz.Name = "BTNsãoJoãodaCruz";
            this.BTNsãoJoãodaCruz.Size = new System.Drawing.Size(172, 214);
            this.BTNsãoJoãodaCruz.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTNsãoJoãodaCruz.TabIndex = 10;
            this.BTNsãoJoãodaCruz.TabStop = false;
            this.BTNsãoJoãodaCruz.Click += new System.EventHandler(this.BTNsãoJoãodaCruz_Click);
            // 
            // BTNsãoJosemaria
            // 
            this.BTNsãoJosemaria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BTNsãoJosemaria.Image = global::Totus_Tuus.Properties.Resources.sao_josemaria;
            this.BTNsãoJosemaria.Location = new System.Drawing.Point(330, 125);
            this.BTNsãoJosemaria.Name = "BTNsãoJosemaria";
            this.BTNsãoJosemaria.Size = new System.Drawing.Size(159, 214);
            this.BTNsãoJosemaria.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTNsãoJosemaria.TabIndex = 9;
            this.BTNsãoJosemaria.TabStop = false;
            this.BTNsãoJosemaria.Click += new System.EventHandler(this.BTNsãoJosemaria_Click);
            // 
            // BTNsãoLuís
            // 
            this.BTNsãoLuís.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BTNsãoLuís.Image = global::Totus_Tuus.Properties.Resources.Sao_Luis_Maria_Grignion_de_Montfort1;
            this.BTNsãoLuís.Location = new System.Drawing.Point(77, 125);
            this.BTNsãoLuís.Name = "BTNsãoLuís";
            this.BTNsãoLuís.Size = new System.Drawing.Size(155, 214);
            this.BTNsãoLuís.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTNsãoLuís.TabIndex = 8;
            this.BTNsãoLuís.TabStop = false;
            this.BTNsãoLuís.Click += new System.EventHandler(this.BTNsãoLuís_Click_1);
            // 
            // FRMlivros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = global::Totus_Tuus.Properties.Resources.rosa_branca;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.panelLivros);
            this.Name = "FRMlivros";
            this.Size = new System.Drawing.Size(817, 420);
            this.panelLivros.ResumeLayout(false);
            this.panelLivros.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTNsãoJoãodaCruz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNsãoJosemaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNsãoLuís)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLivros;
        private System.Windows.Forms.Label lblJoaodaCruz;
        private System.Windows.Forms.Label lblSL;
        private System.Windows.Forms.Label lblJME;
        private System.Windows.Forms.Label lblLIVROS;
        private System.Windows.Forms.PictureBox BTNsãoJoãodaCruz;
        private System.Windows.Forms.PictureBox BTNsãoJosemaria;
        private System.Windows.Forms.PictureBox BTNsãoLuís;
    }
}
