﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Totus_Tuus.Classes.Personagens;
using Totus_Tuus.Classes.Personagens.PersonagemReferencia;

namespace Totus_Tuus.Telas
{
    public partial class FRMlistaDePersonagens : UserControl
    {
        public FRMlistaDePersonagens()
        {
            InitializeComponent();
        }

        private void BTNvoltarParaAddPers_Click(object sender, EventArgs e)
        {
            FRMlistaDePersonagens persList = new FRMlistaDePersonagens();
            FRMpersonagem pers = new FRMpersonagem();
            persList.Controls.Clear();

            if (panelListarPers.Controls.Count > 5)
            {
                panelListarPers.Controls.RemoveAt(panelListarPers.Controls.Count - 1);
            }

            LBLpersoList.Visible = false;
            TXTpesqPersRef.Visible = false;
            IconePesq.Visible = false;
            fotoFlor3.Visible = false;
            fotoFlor4.Visible = false;
            DGVpesquisarPersRef.Visible = false;
            BTNpesquisarPersRef.Visible = false;
            BTNvoltarParaAddPers.Visible = false;
            panelListarPers.Controls.Add(pers);

            
        }

        private void BTNpesquisarPersRef_Click(object sender, EventArgs e)
        {
            BusinessPerRef business = new BusinessPerRef();
            List<ViewPerRef> lista = business.Consultar(TXTpesqPersRef.Text.Trim());


            foreach (ViewPerRef item in lista)
            {
                bindingSource1.Add(item.Referencia);
            }
        

            

            DGVpesquisarPersRef.AutoGenerateColumns = false;
            DGVpesquisarPersRef.DataSource = lista;
        }

        private void DGVpesquisarPersRef_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
    
}
