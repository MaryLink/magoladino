﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Totus_Tuus.Classes.Lectio;
using Totus_Tuus.Classes;

namespace Totus_Tuus.Telas
{
    public partial class FRMmaria : UserControl
    {
   
        public FRMmaria( string  tipo)
        {
            InitializeComponent();
            BusinessLectio bus = new BusinessLectio();
            DGVconsulta.AutoGenerateColumns = false;
            if (tipo == "Jesus Cristo")
            {
                List<VIEWLectio> vw_lectio = bus.listarLectio("Jesus Cristo");
                DGVconsulta.DataSource = vw_lectio;

            }
            if (tipo == "Santa Maria")
            {
                List<VIEWLectio> vw_lectio = bus.listarLectio("Santa Maria");
                DGVconsulta.DataSource = vw_lectio;
                
            }
            if (tipo == "São JóseMaria Escrivá")
            {
                List<VIEWLectio> vw_lectio = bus.listarLectio("São JóseMaria Escrivá");
                DGVconsulta.DataSource = vw_lectio;
               
            }
            if (tipo == "São João da Cruz")
            {
                List<VIEWLectio> vw_lectio = bus.listarLectio("São João da Cruz");
                DGVconsulta.DataSource = vw_lectio;
            }
            if (tipo == "São Luís Maria")
            {
                List<VIEWLectio> vw_lectio = bus.listarLectio("São Luís Maria");
                DGVconsulta.DataSource = vw_lectio;
            }
            if (tipo == "Interior")
            {
                List<VIEWLectio> vw_lectio = bus.listarLectio("Interior");
                DGVconsulta.DataSource = vw_lectio;
            }
           


        }



        private void PesquisarMaria_Click(object sender, EventArgs e)
        {
            
        }

        private void DGVconsulta_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
