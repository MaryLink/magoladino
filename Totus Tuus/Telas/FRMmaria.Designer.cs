﻿namespace Totus_Tuus.Telas
{
    partial class FRMmaria
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRMmaria));
            this.PesquisarMaria = new System.Windows.Forms.PictureBox();
            this.TXTpMaria = new System.Windows.Forms.TextBox();
            this.DGVconsulta = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PesquisarMaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVconsulta)).BeginInit();
            this.SuspendLayout();
            // 
            // PesquisarMaria
            // 
            this.PesquisarMaria.Image = ((System.Drawing.Image)(resources.GetObject("PesquisarMaria.Image")));
            this.PesquisarMaria.Location = new System.Drawing.Point(9, 19);
            this.PesquisarMaria.Name = "PesquisarMaria";
            this.PesquisarMaria.Size = new System.Drawing.Size(50, 42);
            this.PesquisarMaria.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PesquisarMaria.TabIndex = 2;
            this.PesquisarMaria.TabStop = false;
            this.PesquisarMaria.Click += new System.EventHandler(this.PesquisarMaria_Click);
            // 
            // TXTpMaria
            // 
            this.TXTpMaria.Location = new System.Drawing.Point(65, 40);
            this.TXTpMaria.Multiline = true;
            this.TXTpMaria.Name = "TXTpMaria";
            this.TXTpMaria.Size = new System.Drawing.Size(174, 21);
            this.TXTpMaria.TabIndex = 3;
            // 
            // DGVconsulta
            // 
            this.DGVconsulta.AllowUserToAddRows = false;
            this.DGVconsulta.AllowUserToDeleteRows = false;
            this.DGVconsulta.AllowUserToResizeColumns = false;
            this.DGVconsulta.AllowUserToResizeRows = false;
            this.DGVconsulta.BackgroundColor = System.Drawing.Color.White;
            this.DGVconsulta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGVconsulta.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.DGVconsulta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVconsulta.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn1,
            this.dataGridViewTextBoxColumn5,
            this.Column1});
            this.DGVconsulta.GridColor = System.Drawing.Color.White;
            this.DGVconsulta.Location = new System.Drawing.Point(20, 85);
            this.DGVconsulta.Name = "DGVconsulta";
            this.DGVconsulta.ReadOnly = true;
            this.DGVconsulta.RowHeadersVisible = false;
            this.DGVconsulta.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGVconsulta.Size = new System.Drawing.Size(555, 233);
            this.DGVconsulta.TabIndex = 4;
            this.DGVconsulta.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVconsulta_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Livro";
            this.dataGridViewTextBoxColumn1.HeaderText = "Livro";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Titulo";
            this.dataGridViewTextBoxColumn2.HeaderText = "Título";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Personagem_Principal";
            this.dataGridViewTextBoxColumn3.HeaderText = "P.Principal";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Referencia";
            this.dataGridViewTextBoxColumn4.HeaderText = "Referência";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.DataPropertyName = "Personagem";
            this.dataGridViewComboBoxColumn1.HeaderText = "Personagens";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.ReadOnly = true;
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Data";
            this.dataGridViewTextBoxColumn5.HeaderText = "Data";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Mensagem";
            this.Column1.HeaderText = "Mensagem";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(573, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 41);
            this.label1.TabIndex = 52;
            this.label1.Text = "Mensagem";
            // 
            // FRMmaria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::Totus_Tuus.Properties.Resources.mj_desf;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DGVconsulta);
            this.Controls.Add(this.TXTpMaria);
            this.Controls.Add(this.PesquisarMaria);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DoubleBuffered = true;
            this.Name = "FRMmaria";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Size = new System.Drawing.Size(817, 420);
            ((System.ComponentModel.ISupportInitialize)(this.PesquisarMaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVconsulta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox PesquisarMaria;
        private System.Windows.Forms.TextBox TXTpMaria;
        private System.Windows.Forms.DataGridView DGVconsulta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}
