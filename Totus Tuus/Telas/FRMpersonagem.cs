﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Totus_Tuus.Classes;
using Totus_Tuus.Classes.Personagens;
using Totus_Tuus.Classes.Referencia;

namespace Totus_Tuus.Telas
{
    public partial class FRMpersonagem : UserControl
    {

        BindingList<DTOreferencia> referencias = new BindingList<DTOreferencia>();
        List<DTOreferencia> listRefer = new List<DTOreferencia>();

        public FRMpersonagem()
        {
            InitializeComponent();
            ConfigurarGrid();
        }
        void ConfigurarGrid()
        {
            DGVPerRef.AutoGenerateColumns = false;
            DGVPerRef.DataSource = referencias;
        }

        private void BTNtelaBuscarPers_Click_1(object sender, EventArgs e)
        {
            FRMlistaDePersonagens persList = new FRMlistaDePersonagens();
            FRMpersonagem pers = new FRMpersonagem();
            pers.Controls.Clear();

            if (panelAddPersRef.Controls.Count > 5)
            {
                panelAddPersRef.Controls.RemoveAt(panelAddPersRef.Controls.Count - 1);
            }

            LBLnome.Visible = false;
            LBLpers.Visible = false;
            LBLref.Visible = false;
            BTNsalvarRefere.Visible = false;
            BTNsalvarPers.Visible = false;
            BTNtelaBuscarPers.Visible = false;
            fotoFlor2.Visible = false;
            FotoFlor1.Visible = false;
            TXTnomePerson.Visible = false;
            TXTrefPerson.Visible = false;
            DGVPerRef.Visible = false;
            panelAddPersRef.Controls.Add(persList);

        }
        public string Nome { get; set; }

        private void BTNsalvarPers_Click_1(object sender, EventArgs e)
        {
            try
            {

                DTOpersonagem dto = new DTOpersonagem();
                dto.NM_Personagem = TXTnomePerson.Text.Trim().ToLower();

                DTOreferencia dT = new DTOreferencia();
                
                BusinessPersonagens bus = new BusinessPersonagens();
                bus.SalvarPersonagens(dto, referencias.ToList());
                
                MessageBox.Show("Salvo com sucesso");
                TXTnomePerson.Text = string.Empty;
                TXTrefPerson.Text = string.Empty;
                referencias.Clear();
        }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message);
            }
}

        private void BTNsalvarRefere_Click(object sender, EventArgs e)
        {
            
            if (TXTrefPerson.Text != "(none)")
            {
                
                DTOreferencia dto = new DTOreferencia();
                dto.Referencia = TXTrefPerson.Text.Trim().ToLower();
                referencias.Add(dto);
                TXTrefPerson.Text = string.Empty;
            }
            else if (TXTrefPerson.Text == "(none)")
            {
                TXTrefPerson.Text = "(none)";
                DTOreferencia dto = new DTOreferencia();
                dto.Referencia = TXTrefPerson.Text.Trim().ToLower();
                referencias.Add(dto);
                TXTrefPerson.Text = string.Empty;
            }
        }
    }
}
