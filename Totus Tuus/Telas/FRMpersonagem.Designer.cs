﻿namespace Totus_Tuus.Telas
{
    partial class FRMpersonagem
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelAddPersRef = new System.Windows.Forms.Panel();
            this.DGVPerRef = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BTNtelaBuscarPers = new System.Windows.Forms.Label();
            this.BTNsalvarRefere = new System.Windows.Forms.Button();
            this.fotoFlor2 = new System.Windows.Forms.PictureBox();
            this.FotoFlor1 = new System.Windows.Forms.PictureBox();
            this.BTNsalvarPers = new System.Windows.Forms.Button();
            this.TXTrefPerson = new System.Windows.Forms.TextBox();
            this.LBLnome = new System.Windows.Forms.Label();
            this.LBLref = new System.Windows.Forms.Label();
            this.TXTnomePerson = new System.Windows.Forms.TextBox();
            this.LBLpers = new System.Windows.Forms.Label();
            this.panelAddPersRef.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPerRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoFlor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FotoFlor1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelAddPersRef
            // 
            this.panelAddPersRef.BackgroundImage = global::Totus_Tuus.Properties.Resources.rosa_branca;
            this.panelAddPersRef.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelAddPersRef.Controls.Add(this.DGVPerRef);
            this.panelAddPersRef.Controls.Add(this.BTNtelaBuscarPers);
            this.panelAddPersRef.Controls.Add(this.BTNsalvarRefere);
            this.panelAddPersRef.Controls.Add(this.fotoFlor2);
            this.panelAddPersRef.Controls.Add(this.FotoFlor1);
            this.panelAddPersRef.Controls.Add(this.BTNsalvarPers);
            this.panelAddPersRef.Controls.Add(this.TXTrefPerson);
            this.panelAddPersRef.Controls.Add(this.LBLnome);
            this.panelAddPersRef.Controls.Add(this.LBLref);
            this.panelAddPersRef.Controls.Add(this.TXTnomePerson);
            this.panelAddPersRef.Controls.Add(this.LBLpers);
            this.panelAddPersRef.Location = new System.Drawing.Point(0, 0);
            this.panelAddPersRef.Margin = new System.Windows.Forms.Padding(4);
            this.panelAddPersRef.Name = "panelAddPersRef";
            this.panelAddPersRef.Size = new System.Drawing.Size(1089, 517);
            this.panelAddPersRef.TabIndex = 0;
            // 
            // DGVPerRef
            // 
            this.DGVPerRef.AllowUserToAddRows = false;
            this.DGVPerRef.AllowUserToDeleteRows = false;
            this.DGVPerRef.AllowUserToResizeColumns = false;
            this.DGVPerRef.AllowUserToResizeRows = false;
            this.DGVPerRef.BackgroundColor = System.Drawing.Color.White;
            this.DGVPerRef.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DGVPerRef.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVPerRef.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.DGVPerRef.GridColor = System.Drawing.Color.White;
            this.DGVPerRef.Location = new System.Drawing.Point(423, 308);
            this.DGVPerRef.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DGVPerRef.MultiSelect = false;
            this.DGVPerRef.Name = "DGVPerRef";
            this.DGVPerRef.ReadOnly = true;
            this.DGVPerRef.RowHeadersVisible = false;
            this.DGVPerRef.RowTemplate.Height = 24;
            this.DGVPerRef.Size = new System.Drawing.Size(276, 185);
            this.DGVPerRef.TabIndex = 62;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "Referencia";
            this.Column1.HeaderText = "Referência(s)";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // BTNtelaBuscarPers
            // 
            this.BTNtelaBuscarPers.AutoSize = true;
            this.BTNtelaBuscarPers.BackColor = System.Drawing.Color.Transparent;
            this.BTNtelaBuscarPers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNtelaBuscarPers.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNtelaBuscarPers.ForeColor = System.Drawing.Color.White;
            this.BTNtelaBuscarPers.Location = new System.Drawing.Point(657, 0);
            this.BTNtelaBuscarPers.Name = "BTNtelaBuscarPers";
            this.BTNtelaBuscarPers.Size = new System.Drawing.Size(401, 41);
            this.BTNtelaBuscarPers.TabIndex = 61;
            this.BTNtelaBuscarPers.Text = "Buscar personagens salvos...>";
            this.BTNtelaBuscarPers.Click += new System.EventHandler(this.BTNtelaBuscarPers_Click_1);
            // 
            // BTNsalvarRefere
            // 
            this.BTNsalvarRefere.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BTNsalvarRefere.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.BTNsalvarRefere.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTNsalvarRefere.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNsalvarRefere.Font = new System.Drawing.Font("Monotype Corsiva", 13F, System.Drawing.FontStyle.Italic);
            this.BTNsalvarRefere.ForeColor = System.Drawing.Color.White;
            this.BTNsalvarRefere.Location = new System.Drawing.Point(713, 220);
            this.BTNsalvarRefere.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BTNsalvarRefere.Name = "BTNsalvarRefere";
            this.BTNsalvarRefere.Size = new System.Drawing.Size(129, 39);
            this.BTNsalvarRefere.TabIndex = 4;
            this.BTNsalvarRefere.Text = "Adicionar";
            this.BTNsalvarRefere.UseVisualStyleBackColor = true;
            this.BTNsalvarRefere.Click += new System.EventHandler(this.BTNsalvarRefere_Click);
            // 
            // fotoFlor2
            // 
            this.fotoFlor2.Image = global::Totus_Tuus.Properties.Resources._47400c75dfabb111ae6f651ed87a6573_linda_rosa_vermelha_flor__cone_by_vexels;
            this.fotoFlor2.Location = new System.Drawing.Point(951, 372);
            this.fotoFlor2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fotoFlor2.Name = "fotoFlor2";
            this.fotoFlor2.Size = new System.Drawing.Size(139, 145);
            this.fotoFlor2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fotoFlor2.TabIndex = 59;
            this.fotoFlor2.TabStop = false;
            // 
            // FotoFlor1
            // 
            this.FotoFlor1.Image = global::Totus_Tuus.Properties.Resources._47400c75dfabb111ae6f651ed87a6573_linda_rosa_vermelha_flor__cone_by_vexels;
            this.FotoFlor1.Location = new System.Drawing.Point(0, 0);
            this.FotoFlor1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FotoFlor1.Name = "FotoFlor1";
            this.FotoFlor1.Size = new System.Drawing.Size(139, 145);
            this.FotoFlor1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FotoFlor1.TabIndex = 58;
            this.FotoFlor1.TabStop = false;
            // 
            // BTNsalvarPers
            // 
            this.BTNsalvarPers.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BTNsalvarPers.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.BTNsalvarPers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTNsalvarPers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNsalvarPers.Font = new System.Drawing.Font("Monotype Corsiva", 13F, System.Drawing.FontStyle.Italic);
            this.BTNsalvarPers.ForeColor = System.Drawing.Color.White;
            this.BTNsalvarPers.Location = new System.Drawing.Point(571, 263);
            this.BTNsalvarPers.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BTNsalvarPers.Name = "BTNsalvarPers";
            this.BTNsalvarPers.Size = new System.Drawing.Size(129, 39);
            this.BTNsalvarPers.TabIndex = 3;
            this.BTNsalvarPers.Text = "Salvar Personagem";
            this.BTNsalvarPers.UseVisualStyleBackColor = true;
            this.BTNsalvarPers.Click += new System.EventHandler(this.BTNsalvarPers_Click_1);
            // 
            // TXTrefPerson
            // 
            this.TXTrefPerson.Font = new System.Drawing.Font("Monotype Corsiva", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.TXTrefPerson.Location = new System.Drawing.Point(424, 220);
            this.TXTrefPerson.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.TXTrefPerson.Name = "TXTrefPerson";
            this.TXTrefPerson.Size = new System.Drawing.Size(275, 32);
            this.TXTrefPerson.TabIndex = 2;
            // 
            // LBLnome
            // 
            this.LBLnome.AutoSize = true;
            this.LBLnome.BackColor = System.Drawing.Color.Transparent;
            this.LBLnome.Font = new System.Drawing.Font("Monotype Corsiva", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLnome.ForeColor = System.Drawing.Color.White;
            this.LBLnome.Location = new System.Drawing.Point(285, 161);
            this.LBLnome.Name = "LBLnome";
            this.LBLnome.Size = new System.Drawing.Size(126, 41);
            this.LBLnome.TabIndex = 55;
            this.LBLnome.Text = "NOME:";
            // 
            // LBLref
            // 
            this.LBLref.AutoSize = true;
            this.LBLref.BackColor = System.Drawing.Color.Transparent;
            this.LBLref.Font = new System.Drawing.Font("Monotype Corsiva", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLref.ForeColor = System.Drawing.Color.White;
            this.LBLref.Location = new System.Drawing.Point(173, 213);
            this.LBLref.Name = "LBLref";
            this.LBLref.Size = new System.Drawing.Size(237, 41);
            this.LBLref.TabIndex = 54;
            this.LBLref.Text = "REFERÊNCIA:";
            // 
            // TXTnomePerson
            // 
            this.TXTnomePerson.Font = new System.Drawing.Font("Monotype Corsiva", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.TXTnomePerson.Location = new System.Drawing.Point(424, 167);
            this.TXTnomePerson.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.TXTnomePerson.Name = "TXTnomePerson";
            this.TXTnomePerson.Size = new System.Drawing.Size(275, 32);
            this.TXTnomePerson.TabIndex = 1;
            // 
            // LBLpers
            // 
            this.LBLpers.AutoSize = true;
            this.LBLpers.BackColor = System.Drawing.Color.Transparent;
            this.LBLpers.Font = new System.Drawing.Font("Monotype Corsiva", 25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLpers.ForeColor = System.Drawing.Color.White;
            this.LBLpers.Location = new System.Drawing.Point(413, 94);
            this.LBLpers.Name = "LBLpers";
            this.LBLpers.Size = new System.Drawing.Size(292, 51);
            this.LBLpers.TabIndex = 52;
            this.LBLpers.Text = "PERSONAGEM";
            // 
            // FRMpersonagem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::Totus_Tuus.Properties.Resources.rosa_branca;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.panelAddPersRef);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FRMpersonagem";
            this.Size = new System.Drawing.Size(1089, 517);
            this.panelAddPersRef.ResumeLayout(false);
            this.panelAddPersRef.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPerRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoFlor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FotoFlor1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelAddPersRef;
        private System.Windows.Forms.Label BTNtelaBuscarPers;
        private System.Windows.Forms.Button BTNsalvarRefere;
        private System.Windows.Forms.PictureBox fotoFlor2;
        private System.Windows.Forms.PictureBox FotoFlor1;
        private System.Windows.Forms.Button BTNsalvarPers;
        private System.Windows.Forms.TextBox TXTrefPerson;
        private System.Windows.Forms.Label LBLnome;
        private System.Windows.Forms.Label LBLref;
        private System.Windows.Forms.TextBox TXTnomePerson;
        private System.Windows.Forms.Label LBLpers;
        private System.Windows.Forms.DataGridView DGVPerRef;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}
