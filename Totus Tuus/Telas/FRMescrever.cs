﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Totus_Tuus.Telas;
using Totus_Tuus.Classes;
using Totus_Tuus.Classes.Livro;
using Totus_Tuus.Classes.Lectio;
using Totus_Tuus.Classes.Personagens.PersonagemReferencia;
using Totus_Tuus.Classes.Lectio.Lectio_Personagem;
using Totus_Tuus.Classes.usuario;

namespace Totus_Tuus
{
    public partial class FRMescrever : UserControl
    {
        BusinessUsuario user = new BusinessUsuario();
        BindingList<DTOPerRef> dtoPerRef = new BindingList<DTOPerRef>();
        ViewPerRef view = new ViewPerRef();
        BindingList<string> Nome = new BindingList<string>();
        BindingList<string> Referenci = new BindingList<string>();
        public FRMescrever()
        {
            InitializeComponent();
            DGVPerRef.AutoGenerateColumns = false;
            CarregarCombos();
            CarregarNewPerso();
           

        }

        private void BTNnewPerso_Click(object sender, EventArgs e)
        {
            FRMpersonagem tela = new FRMpersonagem();
            FRMescrever escrever = new FRMescrever();
            escrever.Controls.Clear();
            escrever.Visible = false;
            panelEscrever.Controls.Remove(escrever);

            if (panelEscrever.Controls.Count > 5)
            {
                panelEscrever.Controls.RemoveAt(panelEscrever.Controls.Count - 1);
            }

            LBLlivro.Visible = false;
            LBLpersoAdd.Visible = false;
            LBLpPrincipal.Visible = false;
            LBLreferAdd.Visible = false;
            LBLtexto.Visible = false;
            LBLtitulo.Visible = false;
            LBLdestino.Visible = false;
            DGVPerRef.Visible = false;
            CBOaddReferen.Visible = false;
            CBOdestino.Visible = false;
            CBOaddPerso.Visible = false;
            TXTpPrincipal.Visible = false;
            TXTtexto.Visible = false;
            TXTtitulo.Visible = false;
            TXTpPrincipal.Visible = false;
            TXTlivro.Visible = false;
            BTNaddRefer.Visible = false;
            BTNnewPerso.Visible = false;
            BTNsalvarLectio.Visible = false;

            panelEscrever.Controls.Add(tela);

        }

        private void CarregarCombos()
        {
            List<string> lista = new List<string>();
            lista.Add("Santa Maria");
            lista.Add("Jesus Cristo");
            lista.Add("Interior");
            lista.Add("São JóseMaria Escrivá");
            lista.Add("São Luís Maria");
            lista.Add("São João da Cruz");

            CBOdestino.DataSource = lista;
            BusinessPerRef business = new BusinessPerRef();
            List<ViewPerRef> listado = business.Listar();
            string suporte = "0";
            foreach (ViewPerRef item in listado)
            {

                string atual = item.Personagem;

                if (item.Personagem != suporte)
                {
                    Nome.Add(atual);
                }
                else if (Nome.Count == 0)
                {
                    Nome.Add(atual);
                }
                suporte = atual;

            }
            CBOaddPerso.DataSource = Nome;


        }
        BusinessUsuario busii = new BusinessUsuario();

        private void BTNsalvarLectio_Click(object sender, EventArgs e)
        {
            try
            {

                
            DTOlectio lectio = new DTOlectio();
            lectio.NM_PPrincipal = TXTpPrincipal.Text.Trim();
            lectio.Titulo = TXTtitulo.Text.Trim();
            lectio.Mensagem = TXTtexto.Text.Trim();
            string uau = CBOdestino.SelectedItem.ToString();
            lectio.Destino = uau.Trim();
            lectio.Data = DateTime.Now;
            DTOLivro livro = new DTOLivro();
            livro.Nome_Livro = TXTlivro.Text;

            lectio.FK_ID_Usuario = SessaoUsuarioLogado.UsuarioLogado.ID_Usuario;

            BusinessLectioPerson busLecPer = new BusinessLectioPerson();

            BusinessLectio lectiobus = new BusinessLectio();

            int id_lectio = lectiobus.SalvarLectio(lectio, livro, dtoPerRef.ToList());

            MessageBox.Show("Salvo com Sucesso");
            }
            catch (Exception erro)
            {

                MessageBox.Show(erro.Message);
            }

        }

        private void CBOaddPerso_SelectedIndexChanged(object sender, EventArgs e)
        {
            while (Referenci.Count > 0)
            {
                Referenci.Clear();
            }
            if (CBOaddPerso.Items.Count > 0)
            {
                BusinessPerRef business = new BusinessPerRef();
                List<ViewPerRef> listador = business.Consultar2(CBOaddPerso.SelectedItem.ToString());
                string suportes = "0";
                foreach (ViewPerRef item in listador)
                {

                    string atual = item.Referencia.Trim();

                    if (item.Referencia != suportes)
                    {
                        Referenci.Add(atual);
                    }
                    else if (Referenci.Count == 0)
                    {
                        Referenci.Add(atual);
                    }
                    suportes = atual;

                }
                CBOaddReferen.DataSource = Referenci;
            }
        }


        BindingList<DTOpersonagem> apoio = new BindingList<DTOpersonagem>();
        private void btnAddPers_Click(object sender, EventArgs e)
        {
            if (CBOaddReferen.SelectedItem.ToString() != null && CBOaddReferen.SelectedItem.ToString() != "")
            {
                DTOpersonagem dto = new DTOpersonagem();
                dto.NM_Personagem = CBOaddPerso.SelectedItem.ToString();
                bool aceito = true;
                foreach (DTOpersonagem item in apoio)
                {
                    if (item.NM_Personagem == dto.NM_Personagem)
                    {
                        aceito = false;
                    }
                }

                if (aceito == true)
                {
                    apoio.Add(dto);
                }
            }
            

        }
        private void CarregarNewPerso()
        {
            DGVPerRef.DataSource = apoio;
            DGVPerRef.AutoGenerateColumns = false;
        }
        BindingList<DTOreferencia> apoiorefer = new BindingList<DTOreferencia>();
        int id_para_salvarPer_Ref = 0;
        BindingList<DTOgrid> grid = new BindingList<DTOgrid>();
        private void BTNaddRefer_Click(object sender, EventArgs e)
        {
            try
            {

                
                DTOPerRef dto = new DTOPerRef();
                view.Personagem = CBOaddPerso.SelectedItem.ToString();
                view.Referencia = CBOaddReferen.SelectedItem.ToString();

                DTOgrid minigrid = new DTOgrid
                {
                    PersonagemGrid = CBOaddPerso.SelectedItem.ToString(),
                    ReferenciaGrid = CBOaddReferen.SelectedItem.ToString()
                };
                grid.Add(minigrid);


                DGVPerRef.DataSource = grid;
                BusinessPerRef bus = new BusinessPerRef();
                dto.ID = id_para_salvarPer_Ref = bus.ConsultarPersonagem_P_lectio(view);
                dtoPerRef.Add(dto);
            }
            catch (Exception erro)
            {
                MessageBox.Show("Registre ao menos um personagem");
                
            }
        
            
        }

       
        private void DGVPerRef_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void DGVPerRef_CurrentCellChanged(object sender, EventArgs e)
        {

        }
    }
}
