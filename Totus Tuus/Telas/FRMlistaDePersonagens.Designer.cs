﻿namespace Totus_Tuus.Telas
{
    partial class FRMlistaDePersonagens
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRMlistaDePersonagens));
            this.panelListarPers = new System.Windows.Forms.Panel();
            this.BTNvoltarParaAddPers = new System.Windows.Forms.PictureBox();
            this.BTNpesquisarPersRef = new System.Windows.Forms.Button();
            this.TXTpesqPersRef = new System.Windows.Forms.TextBox();
            this.IconePesq = new System.Windows.Forms.PictureBox();
            this.DGVpesquisarPersRef = new System.Windows.Forms.DataGridView();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.fotoFlor3 = new System.Windows.Forms.PictureBox();
            this.fotoFlor4 = new System.Windows.Forms.PictureBox();
            this.LBLpersoList = new System.Windows.Forms.Label();
            this.panelListarPers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTNvoltarParaAddPers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IconePesq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVpesquisarPersRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoFlor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoFlor4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelListarPers
            // 
            this.panelListarPers.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panelListarPers.BackgroundImage = global::Totus_Tuus.Properties.Resources.rosa_branca;
            this.panelListarPers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelListarPers.Controls.Add(this.BTNvoltarParaAddPers);
            this.panelListarPers.Controls.Add(this.BTNpesquisarPersRef);
            this.panelListarPers.Controls.Add(this.TXTpesqPersRef);
            this.panelListarPers.Controls.Add(this.IconePesq);
            this.panelListarPers.Controls.Add(this.DGVpesquisarPersRef);
            this.panelListarPers.Controls.Add(this.fotoFlor3);
            this.panelListarPers.Controls.Add(this.fotoFlor4);
            this.panelListarPers.Controls.Add(this.LBLpersoList);
            this.panelListarPers.Location = new System.Drawing.Point(0, 0);
            this.panelListarPers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelListarPers.Name = "panelListarPers";
            this.panelListarPers.Size = new System.Drawing.Size(1089, 517);
            this.panelListarPers.TabIndex = 0;
            // 
            // BTNvoltarParaAddPers
            // 
            this.BTNvoltarParaAddPers.BackgroundImage = global::Totus_Tuus.Properties.Resources.setaBranca;
            this.BTNvoltarParaAddPers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTNvoltarParaAddPers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNvoltarParaAddPers.Location = new System.Drawing.Point(4, 4);
            this.BTNvoltarParaAddPers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNvoltarParaAddPers.Name = "BTNvoltarParaAddPers";
            this.BTNvoltarParaAddPers.Size = new System.Drawing.Size(89, 52);
            this.BTNvoltarParaAddPers.TabIndex = 61;
            this.BTNvoltarParaAddPers.TabStop = false;
            this.BTNvoltarParaAddPers.Click += new System.EventHandler(this.BTNvoltarParaAddPers_Click);
            // 
            // BTNpesquisarPersRef
            // 
            this.BTNpesquisarPersRef.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BTNpesquisarPersRef.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.BTNpesquisarPersRef.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTNpesquisarPersRef.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNpesquisarPersRef.Font = new System.Drawing.Font("Monotype Corsiva", 13F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNpesquisarPersRef.ForeColor = System.Drawing.Color.White;
            this.BTNpesquisarPersRef.Location = new System.Drawing.Point(776, 175);
            this.BTNpesquisarPersRef.Margin = new System.Windows.Forms.Padding(0);
            this.BTNpesquisarPersRef.Name = "BTNpesquisarPersRef";
            this.BTNpesquisarPersRef.Size = new System.Drawing.Size(103, 41);
            this.BTNpesquisarPersRef.TabIndex = 60;
            this.BTNpesquisarPersRef.Text = "Buscar";
            this.BTNpesquisarPersRef.UseVisualStyleBackColor = true;
            this.BTNpesquisarPersRef.Click += new System.EventHandler(this.BTNpesquisarPersRef_Click);
            // 
            // TXTpesqPersRef
            // 
            this.TXTpesqPersRef.Location = new System.Drawing.Point(325, 181);
            this.TXTpesqPersRef.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TXTpesqPersRef.MaxLength = 50;
            this.TXTpesqPersRef.Multiline = true;
            this.TXTpesqPersRef.Name = "TXTpesqPersRef";
            this.TXTpesqPersRef.Size = new System.Drawing.Size(425, 35);
            this.TXTpesqPersRef.TabIndex = 58;
            // 
            // IconePesq
            // 
            this.IconePesq.BackColor = System.Drawing.Color.Transparent;
            this.IconePesq.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("IconePesq.BackgroundImage")));
            this.IconePesq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IconePesq.Location = new System.Drawing.Point(240, 156);
            this.IconePesq.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IconePesq.Name = "IconePesq";
            this.IconePesq.Size = new System.Drawing.Size(60, 60);
            this.IconePesq.TabIndex = 57;
            this.IconePesq.TabStop = false;
            // 
            // DGVpesquisarPersRef
            // 
            this.DGVpesquisarPersRef.AllowUserToAddRows = false;
            this.DGVpesquisarPersRef.AllowUserToDeleteRows = false;
            this.DGVpesquisarPersRef.AllowUserToResizeColumns = false;
            this.DGVpesquisarPersRef.AllowUserToResizeRows = false;
            this.DGVpesquisarPersRef.BackgroundColor = System.Drawing.Color.White;
            this.DGVpesquisarPersRef.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVpesquisarPersRef.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Referencia,
            this.Column2});
            this.DGVpesquisarPersRef.GridColor = System.Drawing.Color.White;
            this.DGVpesquisarPersRef.Location = new System.Drawing.Point(240, 222);
            this.DGVpesquisarPersRef.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DGVpesquisarPersRef.MultiSelect = false;
            this.DGVpesquisarPersRef.Name = "DGVpesquisarPersRef";
            this.DGVpesquisarPersRef.ReadOnly = true;
            this.DGVpesquisarPersRef.RowHeadersVisible = false;
            this.DGVpesquisarPersRef.RowTemplate.Height = 24;
            this.DGVpesquisarPersRef.Size = new System.Drawing.Size(639, 252);
            this.DGVpesquisarPersRef.TabIndex = 56;
            this.DGVpesquisarPersRef.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVpesquisarPersRef_CellContentClick);
            // 
            // Referencia
            // 
            this.Referencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Referencia.DataPropertyName = "Personagem";
            this.Referencia.HeaderText = "Personagem";
            this.Referencia.Name = "Referencia";
            this.Referencia.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "Referencia";
            this.Column2.DataSource = this.bindingSource1;
            this.Column2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Column2.HeaderText = "Referência";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // fotoFlor3
            // 
            this.fotoFlor3.Image = global::Totus_Tuus.Properties.Resources._47400c75dfabb111ae6f651ed87a6573_linda_rosa_vermelha_flor__cone_by_vexels;
            this.fotoFlor3.Location = new System.Drawing.Point(948, 2);
            this.fotoFlor3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fotoFlor3.Name = "fotoFlor3";
            this.fotoFlor3.Size = new System.Drawing.Size(139, 145);
            this.fotoFlor3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fotoFlor3.TabIndex = 50;
            this.fotoFlor3.TabStop = false;
            // 
            // fotoFlor4
            // 
            this.fotoFlor4.Image = global::Totus_Tuus.Properties.Resources._47400c75dfabb111ae6f651ed87a6573_linda_rosa_vermelha_flor__cone_by_vexels;
            this.fotoFlor4.Location = new System.Drawing.Point(0, 369);
            this.fotoFlor4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fotoFlor4.Name = "fotoFlor4";
            this.fotoFlor4.Size = new System.Drawing.Size(139, 145);
            this.fotoFlor4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fotoFlor4.TabIndex = 49;
            this.fotoFlor4.TabStop = false;
            // 
            // LBLpersoList
            // 
            this.LBLpersoList.AutoSize = true;
            this.LBLpersoList.BackColor = System.Drawing.Color.Transparent;
            this.LBLpersoList.Font = new System.Drawing.Font("Monotype Corsiva", 45F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLpersoList.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LBLpersoList.Location = new System.Drawing.Point(224, 28);
            this.LBLpersoList.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLpersoList.Name = "LBLpersoList";
            this.LBLpersoList.Size = new System.Drawing.Size(614, 92);
            this.LBLpersoList.TabIndex = 0;
            this.LBLpersoList.Text = "Lista de Personagens";
            // 
            // FRMlistaDePersonagens
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.panelListarPers);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRMlistaDePersonagens";
            this.Size = new System.Drawing.Size(1089, 517);
            this.panelListarPers.ResumeLayout(false);
            this.panelListarPers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTNvoltarParaAddPers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IconePesq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVpesquisarPersRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoFlor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoFlor4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelListarPers;
        private System.Windows.Forms.Label LBLpersoList;
        private System.Windows.Forms.PictureBox fotoFlor3;
        private System.Windows.Forms.PictureBox fotoFlor4;
        private System.Windows.Forms.TextBox TXTpesqPersRef;
        private System.Windows.Forms.PictureBox IconePesq;
        private System.Windows.Forms.DataGridView DGVpesquisarPersRef;
        private System.Windows.Forms.Button BTNpesquisarPersRef;
        private System.Windows.Forms.PictureBox BTNvoltarParaAddPers;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column2;
    }
}
