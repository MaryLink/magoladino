﻿namespace Totus_Tuus
{
    partial class FRMescrever
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelEscrever = new System.Windows.Forms.Panel();
            this.TXTpPrincipal = new System.Windows.Forms.TextBox();
            this.BTNnewPerso = new System.Windows.Forms.Button();
            this.LBLdestino = new System.Windows.Forms.Label();
            this.CBOdestino = new System.Windows.Forms.ComboBox();
            this.BTNaddRefer = new System.Windows.Forms.Button();
            this.LBLreferAdd = new System.Windows.Forms.Label();
            this.CBOaddReferen = new System.Windows.Forms.ComboBox();
            this.BTNsalvarLectio = new System.Windows.Forms.Button();
            this.TXTlivro = new System.Windows.Forms.TextBox();
            this.LBLpersoAdd = new System.Windows.Forms.Label();
            this.CBOaddPerso = new System.Windows.Forms.ComboBox();
            this.DGVPerRef = new System.Windows.Forms.DataGridView();
            this.TXTtexto = new System.Windows.Forms.TextBox();
            this.LBLtexto = new System.Windows.Forms.Label();
            this.LBLtitulo = new System.Windows.Forms.Label();
            this.TXTtitulo = new System.Windows.Forms.TextBox();
            this.LBLpPrincipal = new System.Windows.Forms.Label();
            this.LBLlivro = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelEscrever.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPerRef)).BeginInit();
            this.SuspendLayout();
            // 
            // panelEscrever
            // 
            this.panelEscrever.BackgroundImage = global::Totus_Tuus.Properties.Resources.rosa_branca;
            this.panelEscrever.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelEscrever.Controls.Add(this.TXTpPrincipal);
            this.panelEscrever.Controls.Add(this.BTNnewPerso);
            this.panelEscrever.Controls.Add(this.LBLdestino);
            this.panelEscrever.Controls.Add(this.CBOdestino);
            this.panelEscrever.Controls.Add(this.BTNaddRefer);
            this.panelEscrever.Controls.Add(this.LBLreferAdd);
            this.panelEscrever.Controls.Add(this.CBOaddReferen);
            this.panelEscrever.Controls.Add(this.BTNsalvarLectio);
            this.panelEscrever.Controls.Add(this.TXTlivro);
            this.panelEscrever.Controls.Add(this.LBLpersoAdd);
            this.panelEscrever.Controls.Add(this.CBOaddPerso);
            this.panelEscrever.Controls.Add(this.DGVPerRef);
            this.panelEscrever.Controls.Add(this.TXTtexto);
            this.panelEscrever.Controls.Add(this.LBLtexto);
            this.panelEscrever.Controls.Add(this.LBLtitulo);
            this.panelEscrever.Controls.Add(this.TXTtitulo);
            this.panelEscrever.Controls.Add(this.LBLpPrincipal);
            this.panelEscrever.Controls.Add(this.LBLlivro);
            this.panelEscrever.Location = new System.Drawing.Point(0, 0);
            this.panelEscrever.Margin = new System.Windows.Forms.Padding(4);
            this.panelEscrever.Name = "panelEscrever";
            this.panelEscrever.Size = new System.Drawing.Size(1089, 517);
            this.panelEscrever.TabIndex = 1;
            // 
            // TXTpPrincipal
            // 
            this.TXTpPrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.TXTpPrincipal.Location = new System.Drawing.Point(349, 170);
            this.TXTpPrincipal.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.TXTpPrincipal.MaxLength = 45;
            this.TXTpPrincipal.Name = "TXTpPrincipal";
            this.TXTpPrincipal.Size = new System.Drawing.Size(228, 32);
            this.TXTpPrincipal.TabIndex = 3;
            // 
            // BTNnewPerso
            // 
            this.BTNnewPerso.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BTNnewPerso.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.BTNnewPerso.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTNnewPerso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNnewPerso.Font = new System.Drawing.Font("Monotype Corsiva", 13F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNnewPerso.ForeColor = System.Drawing.Color.White;
            this.BTNnewPerso.Location = new System.Drawing.Point(11, 12);
            this.BTNnewPerso.Margin = new System.Windows.Forms.Padding(0);
            this.BTNnewPerso.Name = "BTNnewPerso";
            this.BTNnewPerso.Size = new System.Drawing.Size(235, 41);
            this.BTNnewPerso.TabIndex = 65;
            this.BTNnewPerso.Text = "Adicionar Personagens";
            this.BTNnewPerso.UseVisualStyleBackColor = true;
            this.BTNnewPerso.Click += new System.EventHandler(this.BTNnewPerso_Click);
            // 
            // LBLdestino
            // 
            this.LBLdestino.AutoSize = true;
            this.LBLdestino.BackColor = System.Drawing.Color.Transparent;
            this.LBLdestino.Font = new System.Drawing.Font("Monotype Corsiva", 17F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLdestino.ForeColor = System.Drawing.Color.White;
            this.LBLdestino.Location = new System.Drawing.Point(677, 33);
            this.LBLdestino.Name = "LBLdestino";
            this.LBLdestino.Size = new System.Drawing.Size(110, 36);
            this.LBLdestino.TabIndex = 64;
            this.LBLdestino.Text = "Destino:";
            // 
            // CBOdestino
            // 
            this.CBOdestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBOdestino.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CBOdestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.CBOdestino.FormattingEnabled = true;
            this.CBOdestino.ItemHeight = 26;
            this.CBOdestino.Location = new System.Drawing.Point(803, 36);
            this.CBOdestino.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CBOdestino.Name = "CBOdestino";
            this.CBOdestino.Size = new System.Drawing.Size(148, 34);
            this.CBOdestino.TabIndex = 63;
            // 
            // BTNaddRefer
            // 
            this.BTNaddRefer.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BTNaddRefer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.BTNaddRefer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTNaddRefer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNaddRefer.Font = new System.Drawing.Font("Monotype Corsiva", 13F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNaddRefer.ForeColor = System.Drawing.Color.White;
            this.BTNaddRefer.Location = new System.Drawing.Point(964, 190);
            this.BTNaddRefer.Margin = new System.Windows.Forms.Padding(0);
            this.BTNaddRefer.Name = "BTNaddRefer";
            this.BTNaddRefer.Size = new System.Drawing.Size(111, 34);
            this.BTNaddRefer.TabIndex = 62;
            this.BTNaddRefer.Text = "Adicionar Personagem";
            this.BTNaddRefer.UseVisualStyleBackColor = true;
            this.BTNaddRefer.Click += new System.EventHandler(this.BTNaddRefer_Click);
            // 
            // LBLreferAdd
            // 
            this.LBLreferAdd.AutoSize = true;
            this.LBLreferAdd.BackColor = System.Drawing.Color.Transparent;
            this.LBLreferAdd.Font = new System.Drawing.Font("Monotype Corsiva", 17F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLreferAdd.ForeColor = System.Drawing.Color.White;
            this.LBLreferAdd.Location = new System.Drawing.Point(676, 154);
            this.LBLreferAdd.Name = "LBLreferAdd";
            this.LBLreferAdd.Size = new System.Drawing.Size(141, 36);
            this.LBLreferAdd.TabIndex = 61;
            this.LBLreferAdd.Text = "Referências";
            // 
            // CBOaddReferen
            // 
            this.CBOaddReferen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBOaddReferen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CBOaddReferen.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.CBOaddReferen.FormattingEnabled = true;
            this.CBOaddReferen.ItemHeight = 26;
            this.CBOaddReferen.Location = new System.Drawing.Point(683, 190);
            this.CBOaddReferen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CBOaddReferen.Name = "CBOaddReferen";
            this.CBOaddReferen.Size = new System.Drawing.Size(268, 34);
            this.CBOaddReferen.TabIndex = 60;
            // 
            // BTNsalvarLectio
            // 
            this.BTNsalvarLectio.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BTNsalvarLectio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.BTNsalvarLectio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTNsalvarLectio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNsalvarLectio.Font = new System.Drawing.Font("Monotype Corsiva", 13F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNsalvarLectio.ForeColor = System.Drawing.Color.White;
            this.BTNsalvarLectio.Location = new System.Drawing.Point(493, 465);
            this.BTNsalvarLectio.Margin = new System.Windows.Forms.Padding(0);
            this.BTNsalvarLectio.Name = "BTNsalvarLectio";
            this.BTNsalvarLectio.Size = new System.Drawing.Size(88, 41);
            this.BTNsalvarLectio.TabIndex = 59;
            this.BTNsalvarLectio.Text = "Salvar";
            this.BTNsalvarLectio.UseVisualStyleBackColor = true;
            this.BTNsalvarLectio.Click += new System.EventHandler(this.BTNsalvarLectio_Click);
            // 
            // TXTlivro
            // 
            this.TXTlivro.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.TXTlivro.Location = new System.Drawing.Point(349, 62);
            this.TXTlivro.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.TXTlivro.MaxLength = 100;
            this.TXTlivro.Name = "TXTlivro";
            this.TXTlivro.Size = new System.Drawing.Size(228, 32);
            this.TXTlivro.TabIndex = 1;
            // 
            // LBLpersoAdd
            // 
            this.LBLpersoAdd.AutoSize = true;
            this.LBLpersoAdd.BackColor = System.Drawing.Color.Transparent;
            this.LBLpersoAdd.Font = new System.Drawing.Font("Monotype Corsiva", 17F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLpersoAdd.ForeColor = System.Drawing.Color.White;
            this.LBLpersoAdd.Location = new System.Drawing.Point(677, 73);
            this.LBLpersoAdd.Name = "LBLpersoAdd";
            this.LBLpersoAdd.Size = new System.Drawing.Size(145, 36);
            this.LBLpersoAdd.TabIndex = 56;
            this.LBLpersoAdd.Text = "Personagem";
            // 
            // CBOaddPerso
            // 
            this.CBOaddPerso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBOaddPerso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CBOaddPerso.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.CBOaddPerso.FormattingEnabled = true;
            this.CBOaddPerso.ItemHeight = 26;
            this.CBOaddPerso.Location = new System.Drawing.Point(683, 111);
            this.CBOaddPerso.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CBOaddPerso.Name = "CBOaddPerso";
            this.CBOaddPerso.Size = new System.Drawing.Size(268, 34);
            this.CBOaddPerso.TabIndex = 55;
            this.CBOaddPerso.SelectedIndexChanged += new System.EventHandler(this.CBOaddPerso_SelectedIndexChanged);
            // 
            // DGVPerRef
            // 
            this.DGVPerRef.AllowUserToAddRows = false;
            this.DGVPerRef.AllowUserToDeleteRows = false;
            this.DGVPerRef.AllowUserToResizeColumns = false;
            this.DGVPerRef.AllowUserToResizeRows = false;
            this.DGVPerRef.BackgroundColor = System.Drawing.Color.White;
            this.DGVPerRef.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVPerRef.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.DGVPerRef.GridColor = System.Drawing.Color.White;
            this.DGVPerRef.Location = new System.Drawing.Point(683, 250);
            this.DGVPerRef.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DGVPerRef.Name = "DGVPerRef";
            this.DGVPerRef.ReadOnly = true;
            this.DGVPerRef.RowHeadersVisible = false;
            this.DGVPerRef.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGVPerRef.RowTemplate.Height = 24;
            this.DGVPerRef.Size = new System.Drawing.Size(392, 210);
            this.DGVPerRef.TabIndex = 54;
            this.DGVPerRef.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPerRef_CellDoubleClick);
            this.DGVPerRef.CurrentCellChanged += new System.EventHandler(this.DGVPerRef_CurrentCellChanged);
            // 
            // TXTtexto
            // 
            this.TXTtexto.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTtexto.Location = new System.Drawing.Point(11, 250);
            this.TXTtexto.Margin = new System.Windows.Forms.Padding(4);
            this.TXTtexto.MaxLength = 10000;
            this.TXTtexto.Multiline = true;
            this.TXTtexto.Name = "TXTtexto";
            this.TXTtexto.Size = new System.Drawing.Size(569, 210);
            this.TXTtexto.TabIndex = 4;
            // 
            // LBLtexto
            // 
            this.LBLtexto.AutoSize = true;
            this.LBLtexto.BackColor = System.Drawing.Color.Transparent;
            this.LBLtexto.Font = new System.Drawing.Font("Monotype Corsiva", 17F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLtexto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LBLtexto.Location = new System.Drawing.Point(4, 213);
            this.LBLtexto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLtexto.Name = "LBLtexto";
            this.LBLtexto.Size = new System.Drawing.Size(101, 36);
            this.LBLtexto.TabIndex = 53;
            this.LBLtexto.Text = "TEXTO";
            // 
            // LBLtitulo
            // 
            this.LBLtitulo.AutoSize = true;
            this.LBLtitulo.BackColor = System.Drawing.Color.Transparent;
            this.LBLtitulo.Font = new System.Drawing.Font("Monotype Corsiva", 17F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLtitulo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LBLtitulo.Location = new System.Drawing.Point(233, 111);
            this.LBLtitulo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLtitulo.Name = "LBLtitulo";
            this.LBLtitulo.Size = new System.Drawing.Size(93, 36);
            this.LBLtitulo.TabIndex = 47;
            this.LBLtitulo.Text = "Título:";
            // 
            // TXTtitulo
            // 
            this.TXTtitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.TXTtitulo.Location = new System.Drawing.Point(349, 116);
            this.TXTtitulo.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.TXTtitulo.MaxLength = 45;
            this.TXTtitulo.Name = "TXTtitulo";
            this.TXTtitulo.Size = new System.Drawing.Size(228, 32);
            this.TXTtitulo.TabIndex = 2;
            // 
            // LBLpPrincipal
            // 
            this.LBLpPrincipal.AutoSize = true;
            this.LBLpPrincipal.BackColor = System.Drawing.Color.Transparent;
            this.LBLpPrincipal.Font = new System.Drawing.Font("Monotype Corsiva", 17F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLpPrincipal.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LBLpPrincipal.Location = new System.Drawing.Point(45, 167);
            this.LBLpPrincipal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLpPrincipal.Name = "LBLpPrincipal";
            this.LBLpPrincipal.Size = new System.Drawing.Size(263, 36);
            this.LBLpPrincipal.TabIndex = 49;
            this.LBLpPrincipal.Text = "Personagem Principal:";
            // 
            // LBLlivro
            // 
            this.LBLlivro.AutoSize = true;
            this.LBLlivro.BackColor = System.Drawing.Color.Transparent;
            this.LBLlivro.Font = new System.Drawing.Font("Monotype Corsiva", 17F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLlivro.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LBLlivro.Location = new System.Drawing.Point(233, 59);
            this.LBLlivro.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLlivro.Name = "LBLlivro";
            this.LBLlivro.Size = new System.Drawing.Size(87, 36);
            this.LBLlivro.TabIndex = 51;
            this.LBLlivro.Text = "Livro:";
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "PersonagemGrid";
            this.Column1.HeaderText = "Personagem";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "ReferenciaGrid";
            this.Column2.HeaderText = "Referência";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // FRMescrever
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.BackgroundImage = global::Totus_Tuus.Properties.Resources.rosa_branca;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.panelEscrever);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRMescrever";
            this.Size = new System.Drawing.Size(1089, 517);
            this.panelEscrever.ResumeLayout(false);
            this.panelEscrever.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPerRef)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelEscrever;
        private System.Windows.Forms.Button BTNnewPerso;
        private System.Windows.Forms.Label LBLdestino;
        private System.Windows.Forms.ComboBox CBOdestino;
        private System.Windows.Forms.Button BTNaddRefer;
        private System.Windows.Forms.Label LBLreferAdd;
        private System.Windows.Forms.ComboBox CBOaddReferen;
        private System.Windows.Forms.Button BTNsalvarLectio;
        private System.Windows.Forms.TextBox TXTlivro;
        private System.Windows.Forms.Label LBLpersoAdd;
        private System.Windows.Forms.ComboBox CBOaddPerso;
        private System.Windows.Forms.DataGridView DGVPerRef;
        private System.Windows.Forms.TextBox TXTtexto;
        private System.Windows.Forms.Label LBLtexto;
        private System.Windows.Forms.Label LBLtitulo;
        private System.Windows.Forms.TextBox TXTtitulo;
        private System.Windows.Forms.Label LBLpPrincipal;
        private System.Windows.Forms.Label LBLlivro;
        private System.Windows.Forms.TextBox TXTpPrincipal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}
