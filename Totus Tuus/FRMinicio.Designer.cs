﻿namespace Totus_Tuus
{
    partial class TelaInicial
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TelaInicial));
            this.linha = new System.Windows.Forms.Panel();
            this.panelCentro = new System.Windows.Forms.Panel();
            this.LBLmaria = new System.Windows.Forms.Label();
            this.LBLjesus = new System.Windows.Forms.Label();
            this.BTNJesus2 = new System.Windows.Forms.PictureBox();
            this.BTNmaria2 = new System.Windows.Forms.PictureBox();
            this.FotoINICIAL = new System.Windows.Forms.PictureBox();
            this.BTNjesus = new System.Windows.Forms.PictureBox();
            this.BTNmaria = new System.Windows.Forms.PictureBox();
            this.PainelCima = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BTNrelogar = new System.Windows.Forms.PictureBox();
            this.BTNsair = new System.Windows.Forms.PictureBox();
            this.BTNlivro = new System.Windows.Forms.PictureBox();
            this.BTNlapis = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.BTNcruz = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.BTNinterior = new System.Windows.Forms.PictureBox();
            this.panelCentro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTNJesus2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNmaria2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FotoINICIAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNjesus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNmaria)).BeginInit();
            this.PainelCima.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTNrelogar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNsair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNlivro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNlapis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNcruz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNinterior)).BeginInit();
            this.SuspendLayout();
            // 
            // linha
            // 
            this.linha.BackColor = System.Drawing.Color.LemonChiffon;
            this.linha.Dock = System.Windows.Forms.DockStyle.Top;
            this.linha.Location = new System.Drawing.Point(0, 166);
            this.linha.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.linha.Name = "linha";
            this.linha.Size = new System.Drawing.Size(1089, 9);
            this.linha.TabIndex = 2;
            // 
            // panelCentro
            // 
            this.panelCentro.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panelCentro.BackgroundImage = global::Totus_Tuus.Properties.Resources.rosa_branca_com_os_pingos_de_chuva_no_fundo_preto_34557407;
            this.panelCentro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelCentro.Controls.Add(this.LBLmaria);
            this.panelCentro.Controls.Add(this.LBLjesus);
            this.panelCentro.Controls.Add(this.BTNJesus2);
            this.panelCentro.Controls.Add(this.BTNmaria2);
            this.panelCentro.Controls.Add(this.FotoINICIAL);
            this.panelCentro.Controls.Add(this.BTNjesus);
            this.panelCentro.Controls.Add(this.BTNmaria);
            this.panelCentro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCentro.Location = new System.Drawing.Point(0, 175);
            this.panelCentro.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelCentro.Name = "panelCentro";
            this.panelCentro.Size = new System.Drawing.Size(1089, 517);
            this.panelCentro.TabIndex = 3;
            // 
            // LBLmaria
            // 
            this.LBLmaria.AutoSize = true;
            this.LBLmaria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LBLmaria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBLmaria.Font = new System.Drawing.Font("Monotype Corsiva", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLmaria.ForeColor = System.Drawing.Color.White;
            this.LBLmaria.Location = new System.Drawing.Point(16, 476);
            this.LBLmaria.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.LBLmaria.Name = "LBLmaria";
            this.LBLmaria.Size = new System.Drawing.Size(111, 26);
            this.LBLmaria.TabIndex = 11;
            this.LBLmaria.Text = "Santa Maria";
            // 
            // LBLjesus
            // 
            this.LBLjesus.AutoSize = true;
            this.LBLjesus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LBLjesus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBLjesus.Font = new System.Drawing.Font("Monotype Corsiva", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LBLjesus.ForeColor = System.Drawing.Color.White;
            this.LBLjesus.Location = new System.Drawing.Point(804, 476);
            this.LBLjesus.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.LBLjesus.Name = "LBLjesus";
            this.LBLjesus.Size = new System.Drawing.Size(105, 26);
            this.LBLjesus.TabIndex = 10;
            this.LBLjesus.Text = "Jesus Cristo";
            // 
            // BTNJesus2
            // 
            this.BTNJesus2.BackColor = System.Drawing.Color.Transparent;
            this.BTNJesus2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BTNJesus2.BackgroundImage")));
            this.BTNJesus2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTNJesus2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BTNJesus2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNJesus2.Location = new System.Drawing.Point(804, 340);
            this.BTNJesus2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNJesus2.Name = "BTNJesus2";
            this.BTNJesus2.Size = new System.Drawing.Size(269, 162);
            this.BTNJesus2.TabIndex = 4;
            this.BTNJesus2.TabStop = false;
            this.BTNJesus2.Click += new System.EventHandler(this.BTNJesus2_Click);
            // 
            // BTNmaria2
            // 
            this.BTNmaria2.BackColor = System.Drawing.Color.Transparent;
            this.BTNmaria2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BTNmaria2.BackgroundImage")));
            this.BTNmaria2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTNmaria2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BTNmaria2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNmaria2.Location = new System.Drawing.Point(16, 340);
            this.BTNmaria2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNmaria2.Name = "BTNmaria2";
            this.BTNmaria2.Size = new System.Drawing.Size(265, 162);
            this.BTNmaria2.TabIndex = 5;
            this.BTNmaria2.TabStop = false;
            this.BTNmaria2.Click += new System.EventHandler(this.BTNmaria2_Click);
            // 
            // FotoINICIAL
            // 
            this.FotoINICIAL.BackgroundImage = global::Totus_Tuus.Properties.Resources._23329070_white_rose_with_waterdrops_on_black_background;
            this.FotoINICIAL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.FotoINICIAL.Location = new System.Drawing.Point(0, -9);
            this.FotoINICIAL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FotoINICIAL.Name = "FotoINICIAL";
            this.FotoINICIAL.Size = new System.Drawing.Size(1089, 526);
            this.FotoINICIAL.TabIndex = 3;
            this.FotoINICIAL.TabStop = false;
            // 
            // BTNjesus
            // 
            this.BTNjesus.BackColor = System.Drawing.Color.Transparent;
            this.BTNjesus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BTNjesus.BackgroundImage")));
            this.BTNjesus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTNjesus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNjesus.Location = new System.Drawing.Point(820, 340);
            this.BTNjesus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNjesus.Name = "BTNjesus";
            this.BTNjesus.Size = new System.Drawing.Size(269, 162);
            this.BTNjesus.TabIndex = 1;
            this.BTNjesus.TabStop = false;
            // 
            // BTNmaria
            // 
            this.BTNmaria.BackColor = System.Drawing.Color.Transparent;
            this.BTNmaria.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BTNmaria.BackgroundImage")));
            this.BTNmaria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTNmaria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNmaria.Location = new System.Drawing.Point(12, 340);
            this.BTNmaria.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNmaria.Name = "BTNmaria";
            this.BTNmaria.Size = new System.Drawing.Size(265, 162);
            this.BTNmaria.TabIndex = 2;
            this.BTNmaria.TabStop = false;
            // 
            // PainelCima
            // 
            this.PainelCima.BackColor = System.Drawing.Color.White;
            this.PainelCima.BackgroundImage = global::Totus_Tuus.Properties.Resources.Totus_Tuus_;
            this.PainelCima.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PainelCima.Controls.Add(this.panel1);
            this.PainelCima.Dock = System.Windows.Forms.DockStyle.Top;
            this.PainelCima.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.PainelCima.Location = new System.Drawing.Point(0, 0);
            this.PainelCima.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PainelCima.Name = "PainelCima";
            this.PainelCima.Size = new System.Drawing.Size(1089, 166);
            this.PainelCima.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.BTNrelogar);
            this.panel1.Controls.Add(this.BTNsair);
            this.panel1.Controls.Add(this.BTNlivro);
            this.panel1.Controls.Add(this.BTNlapis);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.BTNcruz);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.BTNinterior);
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1085, 162);
            this.panel1.TabIndex = 3;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // BTNrelogar
            // 
            this.BTNrelogar.BackColor = System.Drawing.Color.Transparent;
            this.BTNrelogar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTNrelogar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNrelogar.Image = ((System.Drawing.Image)(resources.GetObject("BTNrelogar.Image")));
            this.BTNrelogar.Location = new System.Drawing.Point(993, 9);
            this.BTNrelogar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNrelogar.Name = "BTNrelogar";
            this.BTNrelogar.Size = new System.Drawing.Size(39, 37);
            this.BTNrelogar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTNrelogar.TabIndex = 9;
            this.BTNrelogar.TabStop = false;
            this.BTNrelogar.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // BTNsair
            // 
            this.BTNsair.BackColor = System.Drawing.Color.Transparent;
            this.BTNsair.BackgroundImage = global::Totus_Tuus.Properties.Resources.images;
            this.BTNsair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTNsair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNsair.Image = ((System.Drawing.Image)(resources.GetObject("BTNsair.Image")));
            this.BTNsair.Location = new System.Drawing.Point(1040, 9);
            this.BTNsair.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNsair.Name = "BTNsair";
            this.BTNsair.Size = new System.Drawing.Size(39, 37);
            this.BTNsair.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BTNsair.TabIndex = 8;
            this.BTNsair.TabStop = false;
            this.BTNsair.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // BTNlivro
            // 
            this.BTNlivro.BackColor = System.Drawing.Color.Transparent;
            this.BTNlivro.BackgroundImage = global::Totus_Tuus.Properties.Resources.book_bookmarked_black_educational_tool_symbol_icon_icons_com_54585;
            this.BTNlivro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTNlivro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNlivro.Location = new System.Drawing.Point(853, 81);
            this.BTNlivro.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNlivro.Name = "BTNlivro";
            this.BTNlivro.Size = new System.Drawing.Size(113, 74);
            this.BTNlivro.TabIndex = 7;
            this.BTNlivro.TabStop = false;
            this.BTNlivro.Click += new System.EventHandler(this.BTNlivro_Click);
            // 
            // BTNlapis
            // 
            this.BTNlapis.BackColor = System.Drawing.Color.Transparent;
            this.BTNlapis.BackgroundImage = global::Totus_Tuus.Properties.Resources.images;
            this.BTNlapis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTNlapis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNlapis.Location = new System.Drawing.Point(853, 0);
            this.BTNlapis.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNlapis.Name = "BTNlapis";
            this.BTNlapis.Size = new System.Drawing.Size(113, 74);
            this.BTNlapis.TabIndex = 6;
            this.BTNlapis.TabStop = false;
            this.BTNlapis.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BackgroundImage = global::Totus_Tuus.Properties.Resources._47400c75dfabb111ae6f651ed87a6573_linda_rosa_vermelha_flor__cone_by_vexels;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox5.Location = new System.Drawing.Point(707, 0);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(116, 160);
            this.pictureBox5.TabIndex = 5;
            this.pictureBox5.TabStop = false;
            // 
            // BTNcruz
            // 
            this.BTNcruz.BackColor = System.Drawing.Color.Transparent;
            this.BTNcruz.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BTNcruz.BackgroundImage")));
            this.BTNcruz.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTNcruz.Cursor = System.Windows.Forms.Cursors.Cross;
            this.BTNcruz.Location = new System.Drawing.Point(459, -4);
            this.BTNcruz.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNcruz.Name = "BTNcruz";
            this.BTNcruz.Size = new System.Drawing.Size(145, 166);
            this.BTNcruz.TabIndex = 4;
            this.BTNcruz.TabStop = false;
            this.BTNcruz.Click += new System.EventHandler(this.Cruz_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::Totus_Tuus.Properties.Resources._47400c75dfabb111ae6f651ed87a6573_linda_rosa_vermelha_flor__cone_by_vexels;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox3.Location = new System.Drawing.Point(245, 0);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(116, 162);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // BTNinterior
            // 
            this.BTNinterior.BackColor = System.Drawing.Color.Transparent;
            this.BTNinterior.BackgroundImage = global::Totus_Tuus.Properties.Resources._17;
            this.BTNinterior.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTNinterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNinterior.Location = new System.Drawing.Point(-31, -2);
            this.BTNinterior.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNinterior.Name = "BTNinterior";
            this.BTNinterior.Size = new System.Drawing.Size(300, 162);
            this.BTNinterior.TabIndex = 0;
            this.BTNinterior.TabStop = false;
            this.BTNinterior.Click += new System.EventHandler(this.BTNeu_Click);
            // 
            // TelaInicial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1089, 692);
            this.Controls.Add(this.panelCentro);
            this.Controls.Add(this.linha);
            this.Controls.Add(this.PainelCima);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "TelaInicial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inicial";
            this.panelCentro.ResumeLayout(false);
            this.panelCentro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTNJesus2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNmaria2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FotoINICIAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNjesus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNmaria)).EndInit();
            this.PainelCima.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BTNrelogar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNsair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNlivro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNlapis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNcruz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNinterior)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel linha;
        private System.Windows.Forms.Panel panelCentro;
        private System.Windows.Forms.PictureBox BTNinterior;
        private System.Windows.Forms.PictureBox BTNjesus;
        private System.Windows.Forms.PictureBox BTNmaria;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.FlowLayoutPanel PainelCima;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox BTNcruz;
        private System.Windows.Forms.PictureBox BTNlivro;
        private System.Windows.Forms.PictureBox BTNlapis;
        private System.Windows.Forms.PictureBox BTNJesus2;
        private System.Windows.Forms.PictureBox BTNmaria2;
        private System.Windows.Forms.PictureBox FotoINICIAL;
        private System.Windows.Forms.PictureBox BTNsair;
        private System.Windows.Forms.PictureBox BTNrelogar;
        private System.Windows.Forms.Label LBLmaria;
        private System.Windows.Forms.Label LBLjesus;
    }
}

