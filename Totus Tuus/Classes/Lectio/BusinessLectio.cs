﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Totus_Tuus.Classes.Lectio.Lectio_Personagem;
using Totus_Tuus.Classes.Livro;
using Totus_Tuus.Classes.Personagens.PersonagemReferencia;

namespace Totus_Tuus.Classes.Lectio
{
    class BusinessLectio
    {
        DatabaseLectio DatabaseLectio = new DatabaseLectio();
        BusinessLivro Livrobus = new BusinessLivro();
        DataBaseLivro livrod = new DataBaseLivro();

        public int SalvarLectio(DTOlectio dto, DTOLivro dtolivro, List<DTOPerRef> dtoLectio)
        {
            if (dtoLectio.Count == 0)
            {
                throw new ArgumentException("Adicione ao menos um personagem");
            }
            if (dto.Mensagem == string.Empty)
            {
                throw new ArgumentException("Digite a mensagem");
            }
            if (dto.Titulo == string.Empty)
            {
                throw new ArgumentException("Digite o titulo");
            }
            if (dto.Destino == string.Empty)
            {
                throw new ArgumentException("Selecione o destino desejado");
            }
            if (dtolivro.Nome_Livro == string.Empty)
            {
                throw new ArgumentException("Digite o livro");
            }

            int idLivro = livrod.Verificarpk(dtolivro.Nome_Livro);
            if (idLivro == 0)
            {
                idLivro = Livrobus.Salvar(dtolivro);
            }

            DTOLivro ddlivro = new DTOLivro();
            dto.FK_ID_Livro = idLivro;

            int id = DatabaseLectio.SalvarLectio(dto);

            BusinessLectioPerson PerLecBusiness = new BusinessLectioPerson();
            foreach (DTOPerRef item in dtoLectio)
            {
                DatabasePerRef dbPerRef = new DatabasePerRef();

                int idPerRef = dbPerRef.VerificarFKsPerLec(item.ID);

                if (idPerRef == 0)
                {
                    idPerRef = dbPerRef.Salvar(item);
                }

                DTOLectioPerson DTOLecPer = new DTOLectioPerson();

                DTOLecPer.FK_Lectio = id;
                int PerRef = DTOLecPer.FK_Person = idPerRef;
                DatabaseLectioPerson dbLecPerson = new DatabaseLectioPerson();
                PerRef = PerLecBusiness.Salvar_Lectio_FKs(DTOLecPer);
              
            }
            return id;
        }

        public List<VIEWLectio> listarLectio(string destino)
        {
        DatabaseLectio db = new DatabaseLectio();
        List<VIEWLectio> vw_lectio = db.ListarLectio(destino);
            return vw_lectio;
        }

    }

}


