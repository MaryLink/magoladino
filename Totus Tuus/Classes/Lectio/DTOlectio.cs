﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes
{
    class DTOlectio
    {
            public int ID_Lectio { get; set; }
            public string Titulo { get; set; }
            public string NM_PPrincipal { get; set; }
            public string Mensagem { get; set; }
            public DateTime Data { get; set; }
            public string Destino { get; set; }
            public int FK_ID_Usuario { get; set; }
            public int FK_ID_Livro{ get; set; }
    }
}
