﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Lectio
{
    class VIEWLectio
    {
        public int ID_Lectio { get; set; }
        public string Livro { get; set; }
        public string Titulo { get; set; }
        public string Personagem_Principal { get; set; }
        public string Mensagem { get; set; }
        public DateTime Data { get; set; }
        public string Destino { get; set; }
        public string Usuario { get; set; }
        public string Personagem { get; set; }
        public string Referencia { get; set; }

    }
}
