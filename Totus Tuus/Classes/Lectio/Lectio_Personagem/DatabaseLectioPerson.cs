﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Lectio.Lectio_Personagem
{
    class DatabaseLectioPerson
    {
        public int Salvar(DTOLectioPerson dto)
        {
            string script = @"insert into tb_rlc_lectio_personagem_referencia(fk_id_per_ref, fk_id_lectio) values (@fk_id_per_ref, @fk_id_lectio)";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("fk_id_per_ref", dto.FK_Person));
            parms.Add(new MySqlParameter("fk_id_lectio", dto.FK_Lectio));

            DATABASE db = new DATABASE();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<VIEWLectio> ConsultarLECTIO(DTOlectio dto)
        {
            string script = @" SELECT * FROM tb_totustuus
                                WHERE ds_destino = @ds_destino";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_destino", dto.Destino));

            DATABASE db = new DATABASE();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VIEWLectio> listar = new List<VIEWLectio>();

            while (reader.Read())
            {
                VIEWLectio view = new VIEWLectio();
                view.ID_Lectio = reader.GetInt32("id_lectio_divina");
                view.Livro = reader.GetString("nm_livro");
                view.Titulo = reader.GetString("ds_titulo");
                view.Usuario = reader.GetString("nm_usuario");
                view.Personagem_Principal = reader.GetString("nm_personagem_principal");
                view.Personagem = reader.GetString("nm_personagem");
                view.Mensagem = reader.GetString("nm_personagem");
                view.Referencia = reader.GetString("ds_refencia");
                view.Data = reader.GetDateTime("dt_data");
                view.Destino = reader.GetString("ds_destino");

                listar.Add(view);
            }
            reader.Close();

            return listar;


        }

        public int VerificarFKsLectio(DTOLectioPerson dto)
        {
            string script = @"select * from tb_rlc_lectio_personagem_referencia
                            where fk_id_per_ref = @fk_id_per_ref and fk_id_lectio = @fk_id_lectio";
            List<MySqlParameter> parms2 = new List<MySqlParameter>();
            parms2.Add(new MySqlParameter("fk_id_per_ref ", dto.FK_Person));
            parms2.Add(new MySqlParameter("fk_id_lectio", dto.FK_Lectio));


            DATABASE dat = new DATABASE();
            MySqlDataReader reader2 = dat.ExecuteSelectScript(script, parms2);


            int LecPer = 0;
            if (reader2.Read())
            {
                LecPer = reader2.GetInt32("id_rlc_lectio_per_ref");
            }
            reader2.Close();

            return LecPer;
        }
    }
}
