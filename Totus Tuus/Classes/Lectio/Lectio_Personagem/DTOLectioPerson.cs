﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Lectio.Lectio_Personagem
{
    class DTOLectioPerson
    {
        public int ID { get; set; }

        public int FK_Lectio { get; set; }

        public int FK_Person { get; set; }
    }
}
