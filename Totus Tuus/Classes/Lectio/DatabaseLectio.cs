﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Lectio
{
    class DatabaseLectio
    {
        public int SalvarLectio(DTOlectio save)
        {
            string script = @"INSERT INTO tb_lectio_divina(ds_titulo, nm_personagem_principal, ds_mensagem, dt_data, ds_destino, fk_id_usuario, fk_id_livro)
                            VALUES (@ds_titulo, @nm_personagem_principal, @ds_mensagem, @dt_data, @ds_destino, @fk_id_usuario, @fk_id_livro)";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("ds_titulo", save.Titulo));
            parms.Add(new MySqlParameter("nm_personagem_principal", save.NM_PPrincipal));
            parms.Add(new MySqlParameter("ds_mensagem", save.Mensagem));
            parms.Add(new MySqlParameter("dt_data", save.Data));
            parms.Add(new MySqlParameter("ds_destino", save.Destino));
            parms.Add(new MySqlParameter("fk_id_usuario", save.FK_ID_Usuario));
            parms.Add(new MySqlParameter("fk_id_livro", save.FK_ID_Livro));

            DATABASE database = new DATABASE();
            int pk = database.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }




        public List<VIEWLectio> ListarLectio(string Destino)
        {
            string script = @"select * from vw_lectio
                            where ds_destino = @ds_destino and nm_usuario = @nm_usuario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_destino", Destino));
            parms.Add(new MySqlParameter("nm_usuario", SessaoUsuarioLogado.UsuarioLogado.NM_Usuario));

            DATABASE dat = new DATABASE();
            MySqlDataReader reader = dat.ExecuteSelectScript(script, parms);

            List<VIEWLectio> view = new List<VIEWLectio>();
            while (reader.Read())
            {
                VIEWLectio lec = new VIEWLectio();
                lec.ID_Lectio = reader.GetInt32("id_lectio_divina");
                lec.Livro = reader.GetString("nm_livro");
                lec.Titulo = reader.GetString("ds_titulo");
                lec.Personagem_Principal = reader.GetString("nm_personagem_principal");
                lec.Mensagem = reader.GetString("ds_mensagem");
                lec.Data = reader.GetDateTime("dt_data");
                lec.Destino = reader.GetString("ds_destino");
                lec.Usuario = reader.GetString("nm_usuario");
                lec.Personagem = reader.GetString("nm_personagem");
                lec.Referencia = reader.GetString("ds_referencia");

                view.Add(lec);
            }

            reader.Close();

            return view;
        }
    }
}
