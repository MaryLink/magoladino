﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Livro
{
    class DTOLivro
    {
        public int ID_Livro { get; set; }
        public string Nome_Livro{ get; set; }
    }
}
