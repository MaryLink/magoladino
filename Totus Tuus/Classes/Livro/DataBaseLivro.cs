﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Livro
{
    class DataBaseLivro
    {
        public int Salvar(DTOLivro dto)
        {
            string script = @"insert into tb_livro(nm_livro)
                            values (@nm_livro)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_livro",dto.Nome_Livro));

            DATABASE dta = new DATABASE();
            int pk = dta.ExecuteInsertScriptWithPk(script,parms);
            return pk;


        }
        
        public int Verificarpk(string livro)
        {
            string script = @"SELECT * FROM tb_livro 
                            WHERE nm_livro = @nm_livro";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_livro",livro));

            DATABASE dat = new DATABASE();
            MySqlDataReader reader = dat.ExecuteSelectScript(script, parms);

            int livo = 0;
            if (reader.Read()) 
            {
                livo = reader.GetInt32("id_livro");

            }
            reader.Close();
            return livo;

        }



    }
}
