﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Totus_Tuus.Classes.Personagens;
using Totus_Tuus.Classes.Personagens.PersonagemReferencia;

namespace Totus_Tuus.Classes
{
    class databasePersonagens
    {
        DATABASE db = new DATABASE();
        public int SalvarPersonagem(DTOpersonagem dto)
        {
            string script = @"INSERT INTO tb_personagem(nm_personagem )
                                VALUES(@nm_personagem) ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_personagem", dto.NM_Personagem));


            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }

        public List<ViewPersonagem> ListarLectio(string Personagem)
        {
            string script = @"select * from vw_personagem
                            where nm_personagem like @nm_personagem";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_personagem", Personagem + "%"));


            DATABASE dat = new DATABASE();
            MySqlDataReader reader = dat.ExecuteSelectScript(script, parms);

            List<ViewPersonagem> view = new List<ViewPersonagem>();
            while (reader.Read())
            {
                ViewPersonagem lec = new ViewPersonagem();
                lec.Id_Personagem = reader.GetInt32("id_lectio");
                lec.Personagem = reader.GetString("nm_personagem");
                lec.Referencia = reader.GetString("ds_referencia");

                view.Add(lec);
            }

            reader.Close();

            return view;


        }


        public int VerificarPersonagem(string Personagem)
        {
            string script = @"select * from tb_personagem
                            where nm_personagem = @nm_personagem";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_personagem", Personagem));


            DATABASE dat = new DATABASE();
            MySqlDataReader reader = dat.ExecuteSelectScript(script, parms);

            int personagem = 0;
            if (reader.Read())
            {
                personagem = reader.GetInt32("id_personagem");
            }

            reader.Close();

            return personagem;


        }






    }
}
