﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Personagens.PersonagemReferencia
{
    class BusinessPerRef
    {
        public int Salvar(DTOPerRef dto)
        {
            DatabasePerRef db = new DatabasePerRef();
            return db.Salvar(dto);
        }
        public List<ViewPerRef> Consultar(string pesquisa)
        {
            DatabasePerRef db = new DatabasePerRef();
            return db.ListarPersonagens(pesquisa);
        }
        public List<ViewPerRef> Consultar2(string pesquisa)
        {
            DatabasePerRef db = new DatabasePerRef();
            return db.ListarPersonagens(pesquisa);
        }
        public List<ViewPerRef> Listar()
        {
            DatabasePerRef db = new DatabasePerRef();
            return db.ListarPersonagens_P();
        }
        public int ConsultarPersonagem_P_lectio(ViewPerRef view)
        {
            
            DatabasePerRef db = new DatabasePerRef();
            return db.ConsultarPersonagens_P_Lectio(view);
        }
    }
}
