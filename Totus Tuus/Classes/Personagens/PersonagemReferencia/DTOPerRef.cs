﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Personagens.PersonagemReferencia
{
    class DTOPerRef
    {
        public int ID { get; set; }

        public int FK_Personagem { get; set; }

        public int FK_Referencia { get; set; }

    }
}
