﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Personagens.PersonagemReferencia
{
    class DatabasePerRef
    {

        public int Salvar(DTOPerRef dto)
        {
            string script = @"insert into tb_personagem_referencia(fk_personagem, fk_referencia) values (@fk_personagem, @fk_referencia)";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("fk_personagem", dto.FK_Personagem));
            parms.Add(new MySqlParameter("fk_referencia", dto.FK_Referencia));

            DATABASE db = new DATABASE();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public int VerificarFKs(int Referencia, int Personagem)
        {
            string script = @"select * from tb_personagem_referencia
                            where fk_referencia = @fk_referencia and fk_personagem = @fk_personagem ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_referencia", Referencia));
            parms.Add(new MySqlParameter("fk_personagem", Personagem));


            DATABASE dat = new DATABASE();
            MySqlDataReader reader = dat.ExecuteSelectScript(script, parms);


            int referencia = 0;
            if (reader.Read())
            {
                referencia = reader.GetInt32("fk_referencia");
            }
            reader.Close();

            return referencia;
        }
        public int VerificarFKsPerLec(int ID_PER_LEC)
        {
            string script = @"select * from tb_personagem_referencia
                            where id_per_ref = @id_per_ref";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_per_ref", ID_PER_LEC));

            DATABASE dat = new DATABASE();
            MySqlDataReader reader2 = dat.ExecuteSelectScript(script, parms);


            int PerRefID = 0;
            if (reader2.Read())
            {
                PerRefID = reader2.GetInt32("id_per_ref");
            }
            reader2.Close();

            return PerRefID;
        }

        public List<ViewPerRef> ListarPersonagens(string pesquisa)
        {

            string script = @"SELECT * FROM totustuusdb.vw_personagem_referencia 
                            WHERE nm_personagem like @nm_personagem";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_personagem", pesquisa + "%"));

            DATABASE db = new DATABASE();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ViewPerRef> lista = new List<ViewPerRef>();
            while (reader.Read())
            {
                ViewPerRef dto = new ViewPerRef();
                dto.Personagem = reader.GetString("nm_personagem");
                dto.Referencia = reader.GetString("ds_referencia");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<ViewPerRef> ListarPersonagens_P()
        {

            string script = @"SELECT * FROM totustuusdb.vw_personagem_referencia";

            DATABASE db = new DATABASE();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ViewPerRef> lista = new List<ViewPerRef>();
            while (reader.Read())
            {
                ViewPerRef dto = new ViewPerRef();
                dto.Personagem = reader.GetString("nm_personagem");
                dto.Referencia = reader.GetString("ds_referencia");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public int ConsultarPersonagens_P_Lectio(ViewPerRef View)
        {
            DATABASE db = new DATABASE();
            DTOPerRef dto = new DTOPerRef();

            string script = @"SELECT * FROM tb_personagem WHERE nm_personagem = @nm_personagem";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_personagem", View.Personagem));
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            while (reader.Read())
            {
                dto.FK_Personagem = reader.GetInt32("id_personagem");
            }
            reader.Close();

            string script2 = @"SELECT * FROM tb_referencia WHERE ds_referencia = @ds_referencia";


            List<MySqlParameter> parms2 = new List<MySqlParameter>();
            parms2.Add(new MySqlParameter("ds_referencia", View.Referencia));
            MySqlDataReader reader2 = db.ExecuteSelectScript(script2, parms2);

            while (reader2.Read())
            {
                dto.FK_Referencia = reader2.GetInt32("id_referencia");
            }

            reader2.Close();

            string script3 = @"SELECT * FROM tb_personagem_referencia WHERE fk_referencia = @fk_referencia and fk_personagem = @fk_personagem";

            List<MySqlParameter> parms3 = new List<MySqlParameter>();
            parms3.Add(new MySqlParameter("fk_referencia", dto.FK_Referencia));
            parms3.Add(new MySqlParameter("fk_personagem", dto.FK_Personagem));
            MySqlDataReader reader3 = db.ExecuteSelectScript(script3, parms3);

            while (reader3.Read())
            {
                dto.ID = reader3.GetInt32("id_per_ref");
            }

            reader3.Close();
            return dto.ID;
        }

    }
}
