﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Personagens
{
    class ViewPersonagem
    {
        public int Id_Personagem { get; set; }
        public string Personagem { get; set; }
        public string Referencia { get; set; }
    }
}
