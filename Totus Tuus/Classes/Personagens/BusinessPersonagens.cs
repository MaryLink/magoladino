﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Totus_Tuus.Classes.Personagens.PersonagemReferencia;
using Totus_Tuus.Classes.Referencia;
using Totus_Tuus.Telas;

namespace Totus_Tuus.Classes.Personagens
{
    class BusinessPersonagens
    {
        databasePersonagens Personagensdb = new databasePersonagens();


        public int SalvarPersonagens(DTOpersonagem dto, List<DTOreferencia> referencias)
        {


            if (dto.NM_Personagem == string.Empty)
            {
                throw new ArgumentException("Digite o Nome do Personagem");
            }

            databasePersonagens personagemDatabase = new databasePersonagens();
            int idPersonagem = personagemDatabase.VerificarPersonagem(dto.NM_Personagem);

            if (idPersonagem == 0)
            {
                idPersonagem = personagemDatabase.SalvarPersonagem(dto);
            }



            BusinessPerRef PerRefBusiness = new BusinessPerRef();
            foreach (DTOreferencia item in referencias)
            {
                item.Referencia.Trim();
                if (item.Referencia == "")
                {
                    item.Referencia = "(none)";
                }
                databaseReferencia dbrefer = new databaseReferencia();
                
                int idReferencia = dbrefer.VerificarReferencia(item.Referencia);

                if (idReferencia == 0 )
                {
                    idReferencia = dbrefer.SalvarReferencia(item);
                }

                DTOPerRef PerRefDto = new DTOPerRef();

                PerRefDto.FK_Personagem = idPersonagem;
                int Refer = PerRefDto.FK_Referencia = idReferencia;
                DatabasePerRef Referen = new DatabasePerRef();
                Refer = Referen.VerificarFKs(Refer, idPersonagem);
                if (Refer == 0)
                {
                    PerRefBusiness.Salvar(PerRefDto);
                }
            }

            return idPersonagem;


        }
    }
}