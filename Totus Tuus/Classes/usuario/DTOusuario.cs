﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes
{
    class DTOusuario
    {
        public int ID_Usuario { get; set; }

        public string Email { get; set; }

        public string NM_Usuario { get; set; }

        public string Senha_Usuario { get; set; }

        public bool Entrou { get; set; }

    }
}
