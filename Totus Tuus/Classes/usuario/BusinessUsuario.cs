﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.usuario
{
    class BusinessUsuario
    {

        public int Sessao_Atual { get; set; }
        public DTOusuario Registrar(DTOusuario dto)
        {
            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Preencha o campo: Email");
            }
            if (dto.NM_Usuario == string.Empty)
            {
                throw new ArgumentException("Preencha o campo: Nome de usuário");
            }
            if (dto.Senha_Usuario == string.Empty)
            {
                throw new ArgumentException("Preencha o campo: Senha");
            }

            DatabaseUsuario dbuser = new DatabaseUsuario();
            dto.ID_Usuario = dbuser.Salvar(dto);
            return dto;
        }




        public DTOusuario Logou(DTOusuario dtouser)
        {
            DatabaseUsuario dbuser = new DatabaseUsuario();
            dtouser = dbuser.Logar(dtouser);
            Sessao_Atual = dtouser.ID_Usuario;
            return dtouser;
        }
    }
}
