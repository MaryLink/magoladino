﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.usuario
{
    class DatabaseUsuario
    {
        public int Salvar(DTOusuario save)
        {
            string script = @"INSERT INTO tb_usuario(ds_email, nm_usuario, ds_password)
                            VALUES (@ds_email, @nm_usuario, @ds_password)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_email", save.Email));
            parms.Add(new MySqlParameter("nm_usuario", save.NM_Usuario));
            parms.Add(new MySqlParameter("ds_password", save.Senha_Usuario)) ;
         

            DATABASE database = new DATABASE();
            int pk = database.ExecuteInsertScriptWithPk(script, parms);
            return pk;

            
        }
        public DTOusuario Logar(DTOusuario consulta)
        {
            string script = @"SELECT * FROM tb_usuario where ds_email = @ds_email and ds_password = @ds_password";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_email", consulta.Email));
            parms.Add(new MySqlParameter("ds_password", consulta.Senha_Usuario));

            DATABASE db = new DATABASE();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            
            
            if (reader.Read())
            {
                consulta.ID_Usuario = reader.GetInt32("id_usuario");
                consulta.NM_Usuario = reader.GetString("nm_usuario");
                consulta.Email = reader.GetString("ds_email");
                consulta.Entrou = true;
            }
            return consulta;

        }
    }
}
