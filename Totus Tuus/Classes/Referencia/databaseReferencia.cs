﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Totus_Tuus.Classes.Personagens.PersonagemReferencia;

namespace Totus_Tuus.Classes.Referencia
{
    class databaseReferencia
    {
        DATABASE db = new DATABASE();
        public int SalvarReferencia(DTOreferencia dto)
        {
            
            string script = @"INSERT INTO tb_referencia (ds_referencia)
                                VALUES (@ds_referencia)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_referencia", dto.Referencia));


            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }
        public int VerificarReferencia(string Referencia)
        {
            string script = @"select * from tb_referencia
                            where ds_referencia = @ds_referencia";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_referencia", Referencia));


            DATABASE dat = new DATABASE();
            MySqlDataReader reader = dat.ExecuteSelectScript(script, parms);

            int referencia = 0;
            if (reader.Read())
            {
                referencia = reader.GetInt32("id_referencia");
            }

            reader.Close();

            return referencia;

        }


        public void RemoverReferencia(int idReferencia)
        {
            string script = @"DELETE FROM tb_referencia 
                                WHERE @id_referencia";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_referencia", idReferencia));

            db.ExecuteInsertScript(script, parms);

        }
        public List<ViewPerRef> ListarLectio(string Referencia)
        {
            string script = @"select * from vw_referencia
                            where ds_referencia like @ds_referencia";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_referencia", Referencia + "%"));


            DATABASE dat = new DATABASE();
            MySqlDataReader reader = dat.ExecuteSelectScript(script, parms);

            List<ViewPerRef> view = new List<ViewPerRef>();
            while (reader.Read())
            {
                ViewPerRef lec = new ViewPerRef();
                lec.Id_Referencia = reader.GetInt32("id_lectio");
                lec.Personagem = reader.GetString("nm_personagem");
                lec.Referencia = reader.GetString("ds_referencia");

                view.Add(lec);
            }

            reader.Close();

            return view;

        }
    }
}
