﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Totus_Tuus.Classes.Referencia
{
    class BusinessReferencia
    {
        databaseReferencia Referenciadb = new databaseReferencia();
        public int SalvarReferencia(DTOreferencia dto)
        {
            int id = Referenciadb.SalvarReferencia(dto);
            return id;
        }
        
        public void RemoverReferencia (int idReferencia)
        {
            Referenciadb.RemoverReferencia(idReferencia);          
        }


    }
}
