﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Totus_Tuus.Telas;
using Totus_Tuus.Telas.Santos;

namespace Totus_Tuus
{
    public partial class TelaInicial : Form
    {
        
        public TelaInicial()

        {
            InitializeComponent();
           
        }


        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

            FRMmaria maria = new FRMmaria("Interior");
            maria.Controls.Clear();

            FRMescrever escrever = new FRMescrever();
            if (panelCentro.Controls.Count > 5)
            {
                panelCentro.Controls.RemoveAt(panelCentro.Controls.Count - 1);
            }

            panelCentro.Controls.Add(escrever);
            
            
            maria.Visible = false;
            

            escrever.Visible = true;
            LBLjesus.Visible = false;
            LBLmaria.Visible = false;
            BTNmaria.Visible = false;
            BTNjesus.Visible = false;
            FotoINICIAL.Visible = false;
            BTNJesus2.Visible = false;
            BTNmaria2.Visible = false;            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Cruz_Click(object sender, EventArgs e)
        {
            FRMescrever es = new FRMescrever();
            es.Controls.Clear();
            FotoINICIAL.Visible = false;
            panelCentro.Visible = true;
            FotoINICIAL.Visible = true;
            BTNjesus.Visible = true;
            BTNmaria.Visible = true;
            LBLmaria.Visible = true;
            LBLjesus.Visible = true;
            panelCentro.Visible = true;
            BTNmaria.Visible = true;
            BTNjesus.Visible = true;
            FotoINICIAL.Visible = true;
            BTNJesus2.Visible = true;
            BTNmaria2.Visible = true;
          
        }

        private void BTNlivro_Click(object sender, EventArgs e)
        {
            FRMlivros livros = new FRMlivros();
            if (panelCentro.Controls.Count > 5)
            {
                panelCentro.Controls.RemoveAt(panelCentro.Controls.Count - 1);
            }

            panelCentro.Controls.Add(livros);
            livros.Visible = true;

            LBLjesus.Visible = false;
            LBLmaria.Visible = false;
            BTNmaria.Visible = false;
            BTNjesus.Visible = false;
            FotoINICIAL.Visible = false;
            BTNJesus2.Visible = false;
            BTNmaria2.Visible = false;

        }

        private void BTNmaria2_Click(object sender, EventArgs e)
        {


            FRMescrever es = new FRMescrever();
            es.Controls.Clear();
            FRMmaria maria = new FRMmaria("Santa Maria");
            if (panelCentro.Controls.Count >5)
            {
                panelCentro.Controls.RemoveAt(panelCentro.Controls.Count - 1);
            }

            panelCentro.Controls.Add(maria);
            maria.Visible = true;

            LBLjesus.Visible = false;
            LBLmaria.Visible = false;
            BTNmaria.Visible = false;
            BTNjesus.Visible = false;
            FotoINICIAL.Visible = false;
            BTNJesus2.Visible = false;
            BTNmaria2.Visible = false;
            maria.BackgroundImage = Properties.Resources.mj_desf;
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
           
        }

        private void BTNJesus2_Click(object sender, EventArgs e)
        {

            FRMescrever escrever = new FRMescrever();
            escrever.Controls.Clear();
            FRMmaria maria = new FRMmaria("Jesus Cristo");
     
            if (panelCentro.Controls.Count > 5)
            {
                panelCentro.Controls.RemoveAt(panelCentro.Controls.Count - 1);
            }

            panelCentro.Controls.Add(maria);

            LBLjesus.Visible = false;
            LBLmaria.Visible = false;
            BTNmaria.Visible = false;
            BTNjesus.Visible = false;
            FotoINICIAL.Visible = false;
            BTNJesus2.Visible = false;
            BTNmaria2.Visible = false;
          
            maria.BackgroundImage = Properties.Resources.Jesus_desf;
            
        }

        private void BTNeu_Click(object sender, EventArgs e)
        {        
            FRMescrever escrever = new FRMescrever();
            escrever.Controls.Clear();

            FRMmaria maria = new FRMmaria("Interior");

            if (panelCentro.Controls.Count > 5)
            {
                panelCentro.Controls.RemoveAt(panelCentro.Controls.Count - 1);
            }

            panelCentro.Controls.Add(maria);

            LBLjesus.Visible = false;
            LBLmaria.Visible = false;
            BTNmaria.Visible = false;
            BTNjesus.Visible = false;
            FotoINICIAL.Visible = false;
            BTNJesus2.Visible = false;
            BTNmaria2.Visible = false;
           
            maria.BackgroundImage = Properties.Resources.maria_desfoc;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

       
    }
}
